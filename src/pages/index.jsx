import React from 'react';
import { Link, graphql } from 'gatsby';
import Img from 'gatsby-image';
import Slider from 'react-slick';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import { Layout, Contacts } from '../components';

const MainPage = ({ data }) => (
  <Layout isMainPage>
    <FirstScreenSection />
    <WeHelpSection />
    <TrustedSection />
    <ExpertiseSection />
    <CaseStudiesSection caseData={data.getCase.edges} />
    <ReviewsSection />
    <BlogSection blogData={data.getBlog.edges} />
    <ContactsSection />
  </Layout>
);

const FirstScreenSection = () => (
  <div className="first-screen-section">
    <div className="container">
      <div className="first-screen-wrapper">
        <h1>
          Boost Your Website <br /> With JAMstack
        </h1>
        <h2>crazy fast, better secure & cost-effective</h2>
        <AnchorLink href="#contacts-id">
          <button className="btn btn--trasparent btn--white">
            <strong>Build my product</strong>
            <span>
              <img src="/images/svgs/arrow.svg" alt="Arrow" />
            </span>
          </button>
        </AnchorLink>
        <div className="btn-scroll-down">
          <AnchorLink href="#we-help-id" offset={() => 200}>
            <p>Scroll Down</p>
            <button>
              <img src="/images/svgs/scroll-down.svg" alt="Scroll Down" />
            </button>
          </AnchorLink>
        </div>
      </div>
    </div>
  </div>
);

const weHelpSliderMobileSettings = {
  autoplay: true,
  autoplaySpeed: 4000,
  dots: false,
  infinite: true,
  arrows: false,
  speed: 600,
  fade: true,
};

const weHelpSliderSettings = {
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  infinite: true,
  arrows: false,
  speed: 600,
  fade: true,
};

const WeHelpSection = () => (
  <div id="we-help-id" className="we-help-section">
    <div className="container">
      <div className="row">
        <div className="col-lg-7">
          <h3 className="section-title">
            We help companies win <br /> a race of web performance
          </h3>
          <div className="quote">Why JAMstack & Static Site Generators?</div>
          <p>
            Devhance offers a new approach for building much more faster, affordably scalable and
            SEO-friendly web apps. No more additional expenses for security, maintenance or hosting.
            Zero headaches with content management using Headless CMS.
          </p>
          <div className="mobile-slider slider-mobile d-block d-md-none">
            <Slider {...weHelpSliderMobileSettings}>
              <div className="circle">
                <span className="number">12+</span>
                <span className="description">
                  Clients increased revenue 2x times <br />
                  due to better web performance
                </span>
              </div>
              <div className="circle">
                <span className="number">20+</span>
                <span className="description">
                  Our customers migrated to JAMstack <br />
                  and tripled their web traffic
                </span>
              </div>
              <div className="circle">
                <span className="number">30k</span>
                <span className="description">
                  Hours of productive work <br />
                  and delivering excellence
                </span>
              </div>
            </Slider>
          </div>
          <AnchorLink href="#contacts-id">
            <button className="btn btn--cyan">Scale My Business</button>
          </AnchorLink>
        </div>

        <div className="col-lg-5">
          <div className="slider slider-desktop">
            <Slider {...weHelpSliderSettings}>
              <div className="circle">
                <span className="number">12+</span>
                <span className="description">
                  Clients increased revenue 2x times <br />
                  due to better web performance
                </span>
              </div>
              <div className="circle">
                <span className="number">20+</span>
                <span className="description">
                  Our customers migrated to JAMstack <br />
                  and tripled their web traffic
                </span>
              </div>
              <div className="circle">
                <span className="number">30k</span>
                <span className="description">
                  Hours of productive work <br />
                  and delivering excellence
                </span>
              </div>
            </Slider>
          </div>
        </div>
      </div>
    </div>
  </div>
);

const TrustedSection = () => (
  <div className="trusted-section">
    <div className="container">
      <div className="section-title section-title--small">Trusted by</div>
      <div className="clients">
        <div className="clients-logo d-flex align-items-center">
          <img src="/images/svgs/trusted-by/tefl-logo.png" alt="TEFL Iberia" />
        </div>
        <div className="clients-logo d-flex align-items-center">
          <img src="/images/svgs/trusted-by/myvisiticeland.svg" alt="MyVisitIceland" />
        </div>
        <div className="clients-logo d-flex align-items-center">
          <img src="/images/svgs/trusted-by/befactor-logo.png" alt="Befactor" />
        </div>
        <div className="clients-logo d-flex align-items-center">
          <img src="/images/svgs/trusted-by/rnyso.svg" alt="RNYSO" />
        </div>
      </div>
    </div>
  </div>
);

const ExpertiseSection = () => (
  <div id="expertise-id" className="expertise-section">
    <div className="container">
      <div className="col-lg-12">
        <div className="section-title section-title--white">Expertise</div>
        <div className="section-subtitle section-subtitle--white">
          We focus on innovative solutions & top-notch work <br />
          for companies all over the globe.
        </div>

        <ul>
          <li>
            <img src="/images/svgs/checkmark.svg" alt="Checkmark" />
            Custom UX/UI Design
          </li>
          <li>
            <img src="/images/svgs/checkmark.svg" alt="Checkmark" />
            JAMstack Websites & Web Apps
          </li>
          <li>
            <img src="/images/svgs/checkmark.svg" alt="Checkmark" />
            Progressive Web Applications
          </li>
          <li>
            <img src="/images/svgs/checkmark.svg" alt="Checkmark" />
            Support Services
          </li>
        </ul>
      </div>
    </div>
  </div>
);

const CaseStudiesSection = ({ caseData }) => (
  <div id="case-studies-id" className="case-studies-section">
    <div className="container">
      <div className="col-lg-12">
        <div className="section-title">Case Studies</div>

        {caseData.map(({ node }, index) =>
          index % 2 ? (
            <div className="case row" key={index}>
              <div className="col-lg-5 col-md-6 order-2 order-md-1 case__description">
                <div className="title">{node.frontmatter.cardTitle}</div>
                <p>{node.frontmatter.caseDescription}</p>
                <Link to={node.fields.slug} title={node.frontmatter.cardTitle}>
                  <button className="btn btn--cyan--trasparent">View This Case</button>
                </Link>
              </div>

              <div className="col-lg-6 offset-lg-1 col-md-6 order-1 order-md-2 case__image">
                <Img
                  fluid={node.frontmatter.caseImage.childImageSharp.fluid}
                  alt={`Photo - ${node.frontmatter.cardTitle}`}
                  title={node.frontmatter.cardTitle}
                />
              </div>
            </div>
          ) : (
            <div className="case row" key={index}>
              <div className="col-lg-6 col-md-6 case__image">
                <Img
                  fluid={node.frontmatter.caseImage.childImageSharp.fluid}
                  alt={`Photo - ${node.frontmatter.cardTitle}`}
                  title={node.frontmatter.cardTitle}
                />
              </div>

              <div className="col-lg-5 offset-lg-1 col-md-6 case__description">
                <div className="title">{node.frontmatter.cardTitle}</div>
                <p>{node.frontmatter.caseDescription}</p>
                <Link to={node.fields.slug} title={node.frontmatter.cardTitle}>
                  <button className="btn btn--cyan--trasparent">View This Case</button>
                </Link>
              </div>
            </div>
          )
        )}

        <Link to="/case-studies/" title="Case Studies">
          <button className="btn btn--cyan">See All Cases</button>
        </Link>
      </div>
    </div>
  </div>
);

function SampleNextArrow(props) {
  const { onClick } = props;
  return (
    <button className="arrow next-arrow" aria-label="next-arrow" onClick={onClick} type="button" />
  );
}

function SamplePrevArrow(props) {
  const { onClick } = props;
  return (
    <button className="arrow prev-arrow" aria-label="prev-arrow" onClick={onClick} type="button" />
  );
}

const reviewsSliderSettings = {
  dots: false,
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 1,
  prevArrow: <SampleNextArrow />,
  nextArrow: <SamplePrevArrow />,
  speed: 1500,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        arrows: false,
        dots: true,
        slidesToShow: 1,
        speed: 500,
        // fade: true
      },
    },
  ],
};

const ReviewsSection = () => (
  <div className="reviews-section">
    <div className="container">
      <div className="row">
        <div className="col-lg-4">
          <div className="section-title section-title--white">Reviews</div>
          <div className="section-subtitle section-subtitle--white">
            Communication is a key to success.
            <br />
            What clients say about us
          </div>
        </div>

        <div className="col-12 col-lg-8">
          <div className="slider">
            <Slider {...reviewsSliderSettings}>
              <div className="review-card">
                <div className="wrapper">
                  <p className="feedback">
                    "Devhance was able to revamp the website successfully. The results were a more
                    seamless user experience and a visually aesthetic layout and design.
                    Stakeholders were delighted about the easier navigation and user-friendliness of
                    the platform."
                  </p>
                  <div>
                    <strong className="feedback-speaker">
                      Richard Davie,
                      <br /> Director at TEFL Iberia
                    </strong>
                    <br />
                    <span className="project-name">
                      <a href="//clutch.co/profile/devhance#reviews" target="_blank">
                        Read full review on clutch +
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div className="review-card">
                <div className="wrapper">
                  <p className="feedback">
                    "The new site is a vast improved on the previous iteration and we are happy with
                    the result. The efficient workflow was complimented by Devhance's ability to
                    quickly identify and rectify issues. They are very quick to answer requests and
                    put them into effect. I never had to wait a few days for a request to be
                    answered, researched and implemented."
                  </p>
                  <div>
                    <strong className="feedback-speaker">
                      Höskuldur Jónsson,
                      <br /> Project Manager at Ferdavefir
                    </strong>
                    <br />
                    <span className="project-name">
                      <a href="//clutch.co/profile/devhance#reviews" target="_blank">
                        Read full review on clutch +
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div className="review-card">
                <div className="wrapper">
                  <p className="feedback">
                    "Devhance provided a high quality of work. They come across as a professional
                    outfit who take pride in the work they deliver. The team had great project
                    management skills and kept strong documentation on what work was in scope. They
                    showed in-depth expertise in website development and were very responsive
                    throughout the process."
                  </p>
                  <div>
                    <strong className="feedback-speaker">
                      Jinesh Vohra,
                      <br /> CEO at Mortgage FinTech Company
                    </strong>
                    <br />
                    <span className="project-name">
                      <a href="//clutch.co/profile/devhance#reviews" target="_blank">
                        Read full review on clutch +
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div className="review-card">
                <div className="wrapper">
                  <p className="feedback">
                    "Most impressive which we found about this company is the quality and product
                    delivery speed. Despite the complexity of the project, Devhance team has managed
                    to adapt to our rapidly changing working environment and deliver the high
                    quality stand alone product in very tight timescale. Friendly and responsive
                    team performed at the highest professional level."
                  </p>
                  <div>
                    <strong className="feedback-speaker">
                      NDA Author,
                      <br /> Founder at Cybersecurity Firm
                    </strong>
                    <br />
                    <span className="project-name">
                      <a href="//clutch.co/profile/devhance#reviews" target="_blank">
                        Read full review on clutch +
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div className="review-card">
                <div className="wrapper">
                  <p className="feedback">
                    "Devhance did a tremendous work by speeding up our website. Web pages were
                    loading quite slow and we wanted to improve it. After 4 weeks I was really
                    impressed with the new result. Devhance didn't only provide redevelopment, they
                    also fixed a lot of things that weren't working before. Orchestra staff wants to
                    say a huge thanks for the practical approach and work done!"
                  </p>
                  <div>
                    <strong className="feedback-speaker">
                      D. Bochkareva,
                      <br /> PR Manager at RNYSO
                    </strong>
                    <br />
                    <span className="project-name">RNYSO</span>
                  </div>
                </div>
              </div>
            </Slider>
          </div>
        </div>
      </div>
    </div>
  </div>
);

const BlogSection = ({ blogData }) => (
  <div className="blog-section">
    <div className="container">
      <div className="col-lg-12">
        <div className="section-title">Blog</div>
        <div className="row">
          {blogData.map((post) => (
            <div
              className="col-lg-4 col-md-6 blog-card"
              key={post.node.id}
              itemScope
              itemType="http://schema.org/Article"
            >
              <Link to={post.node.fields.slug} title={post.node.frontmatter.title} itemProp="url">
                <Img
                  fluid={post.node.frontmatter.image.childImageSharp.fluid}
                  alt={`Photo - ${post.node.frontmatter.title}`}
                  title={post.node.frontmatter.title}
                />
                <span className="blog-card__date" itemProp="datePublished">
                  {post.node.frontmatter.date}
                </span>
                <h4 className="blog-card__title" itemProp="name">
                  {post.node.frontmatter.title}
                </h4>
                <p className="blog-card__caption" itemProp="description">
                  {post.node.frontmatter.description.substring(0, 130)}...
                </p>
              </Link>
              <Link to={post.node.fields.slug} title={post.node.frontmatter.title} itemProp="url">
                <button className="btn btn--cyan--trasparent">Read More</button>
              </Link>
            </div>
          ))}
        </div>

        <Link to="/blog/" title="Blog">
          <button className="btn btn--cyan">See More Posts +</button>
        </Link>
      </div>
    </div>
  </div>
);

const ContactsSection = () => (
  <div id="contacts-id" className="contacts-section">
    <Contacts />
  </div>
);

export default MainPage;

export const pageQuery = graphql`
  query MainPage {
    getBlog: allMarkdownRemark(
      limit: 3
      sort: { fields: frontmatter___date, order: DESC }
      filter: { frontmatter: { templateKey: { eq: "blog-post" } } }
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            description
            date(formatString: "MMMM DD, YYYY")
            image {
              childImageSharp {
                fluid(maxWidth: 340, maxHeight: 305, quality: 95) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }

    getCase: allMarkdownRemark(
      limit: 2
      sort: { fields: frontmatter___date, order: DESC }
      filter: {
        frontmatter: { templateKey: { eq: "case-page" } }
        fields: { slug: { ne: "/case-studies/harver/" } }
      }
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            cardTitle
            caseDescription
            caseImage {
              childImageSharp {
                fluid(maxWidth: 525, maxHeight: 467, quality: 96) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
