import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { Layout, Contacts } from '../components';

const TermsAndPrivacy = (props) => (
  <Layout
    title="Terms of Conditions & Privacy Policy"
    description='These Terms of Service and all other legal documents incorporated by reference (collectively, the "Terms") set forth the legal contract between each visitor ("User" or "you" or "your") and Devhance.'
    url={props.location.href}
  >
    <div className="first-screen-section first-screen-section--swap">
      <div className="container">
        <div className="first-screen-wrapper">
          <h1>Terms & Privacy</h1>
          <AnchorLink href="#contacts-section-id">
            <button className="btn btn--trasparent btn--white">Contact us</button>
          </AnchorLink>
          <div className="btn-scroll-down">
            <AnchorLink href="#terms-privacy-content" offset={() => 200}>
              <p>Scroll Down</p>
              <button>
                <img src="/images/svgs/scroll-down.svg" alt="Scroll Down" />
              </button>
            </AnchorLink>
          </div>
        </div>
      </div>
    </div>

    <div id="terms-privacy-content" className="terms-privacy__content">
      <div className="container">
        <div className="info-block">
          <h2>Terms of Use</h2>

          <div>
            <h3>Acceptance of terms:</h3>
            <p>
              PLEASE READ THE FOLLOWING TERMS AND CONDITIONS RELATING TO YOUR USE OF THIS WEBSITE
              CAREFULLY. BY USING THIS WEBSITE, YOU ARE DEEMED TO HAVE AGREED TO THESE TERMS AND
              CONDITIONS AND OUR PRIVACY POLICY, WHICH IS INCORPORATED HEREIN BY REFERENCE. WE
              RESERVE THE RIGHT TO CHANGE THESE TERMS AND CONDITIONS AND THE PRIVACY POLICY AT ANY
              TIME. YOU SHOULD CHECK THESE TERMS AND CONDITIONS PERIODICALLY FOR CHANGES. BY USING
              THIS WEBSITE AFTER WE POST ANY CHANGES TO THESE TERMS AND CONDITIONS, YOU AGREE TO
              ACCEPT THOSE CHANGES WHETHER OR NOT YOU HAVE REVIEWED THEM. IF AT ANY TIME YOU CHOOSE
              NOT TO ACCEPT THESE TERMS AND CONDITIONS PLEASE DO NOT USE THIS WEBSITE.
            </p>
          </div>

          <div>
            <h3>Description and use of this website:</h3>
            <p>
              Devhance requires that all visitors to this World Wide Website owned, operated,
              licensed, and controlled by Devhance (the “Website”) adhere to the following Terms And
              Conditions of Use. Devhance may change, suspend or discontinue any aspect of the
              Website at any time, including the availability of any feature, database or content.
              Devhance may also impose limits on certain features and services and/or restrict your
              access to parts or all of the Website without notice or liability of any kind. To the
              extent that you and Devhance have entered into a client’s service/license agreement
              (“Client Agreement”) for any portion of the Website, the terms and conditions of the
              Client Agreement will control in the event that of any provisions that may conflict
              with these Terms and Conditions of Use.
            </p>
            <p>
              This Website provides authorized users with access to a collection of resources,
              including access to certain service deliverables, articles, and insights for general
              informational purposes. The Content (as such term is defined below) is to be used
              solely as a research tool and not as specific guides for action. YOU UNDERSTAND AND
              AGREE THAT THIS WEBSITE AND THE CONTENT IS PROVIDED “AS-IS” AND YOU ASSUME FULL RISK
              FOR ANY AND ALL USE OF THIS WEBSITE AND FOR ANALYSIS OF THE CONTENT. You may not use
              this Website or the Content for any illegal purpose or in any manner inconsistent with
              these Terms and Conditions of Use. We make every effort to ensure that the Content on
              this Website is accurate and up to date, however accuracy cannot be guaranteed.
            </p>
            <p>
              You are responsible for obtaining access to the Service and that access may involve
              third party fees (such as Internet service provider or airtime charges). In addition,
              you must provide and are responsible for all equipment necessary to access the
              Website. No installation, implementation, customization, consultation, support or
              similar services are included within the scope of these Terms and Conditions of Use.
            </p>
          </div>

          <div>
            <h3>Forums/Blogs:</h3>
            <p>
              You may access certain “forums” or “blogs” through the Website. You may not post on
              any forum/blog any material that is libelous, defamatory, obscene, abusive, invades a
              person’s privacy, violates any intellectual property rights, or that would otherwise
              violate any law. You acknowledge that you are responsible for whatever material you
              submit, and you, not Devhance, have full responsibility for the message, including its
              legality, reliability, appropriateness, originality, and copyright. You may not post
              material that solicits funds, or that advertises or solicits goods or services. You
              may not post material known to be false. You may not post or transmit any information,
              software or other material that contains a virus or other harmful component. We also
              reserve the right to remove any such violative material. All remarks, suggestions,
              ideas, graphics or other information communicated through the Website will become,
              once posted, the property of Devhance (“Posting”). Devhance will not be required to
              treat any Posting as confidential. Devhance will be entitled to use the Postings for
              any commercial or other purpose whatsoever, without compensation to you or any other
              person. You agree to indemnify, defend and hold Devhance harmless from any liability
              arising due to the use or distribution of your Postings. You further grant Devhance
              the right to use your name in connection with the reproduction or distribution of your
              Postings.
            </p>
          </div>

          <div>
            <h3>Modifications to website:</h3>
            <p>
              We reserve the right at any time to modify or discontinue, temporarily or permanently,
              this Website (or any part thereof) with or without notice. You agree that we shall not
              be liable to you or to any third party for any modification, suspension or
              discontinuance of this Website.
            </p>
          </div>

          <div>
            <h3>Agreement:</h3>
            <p>
              By using the Website, you are deemed to agree to these Terms and Conditions of Use.
            </p>
          </div>
        </div>

        <div className="info-block">
          <h2>Privacy Policy</h2>

          <div>
            <h3>Contact information for privacy</h3>
            <p>
              If you have any questions regarding your personal data, feel free to contact our Data
              Protection Officer via devhance.team@gmail.com.
            </p>
          </div>

          <div>
            <h3>Sources of your personal data</h3>
            <p>Devhance may collect and process information that:</p>
            <ul>
              <li>
                you personally provide, for example, in hard copies, via emails or by sending
                personal messages directly to our representatives,
              </li>
              <li>collected during performance of a contract between you and Devhance,</li>
              <li>
                you provide during interaction with our web sites, for example, through cookies
                (please see Cookie policy for details) and web forms,
              </li>
              <li>
                obtained from third parties, for example, recruitment agencies, your
                representatives.
              </li>
            </ul>
          </div>

          <div>
            <h3>Purposes of processing</h3>
            <p>
              Devhance may process your personal data linked to particular purposes and
              corresponding legal bases outlined below:
            </p>
            <ul>
              <li>
                purposes to which you agree in a consent form or purposes that are genuinely
                compatible with them, taking into account your expectations as a data subject when
                consenting to processing;
              </li>
              <li>for the fulfilment of the contract as further specified in that contract;</li>
              <li>
                in order to comply with legal obligations specified in a particular law or
                regulation;
              </li>
              <li>
                legitimate interests pursued by Devhance group members, except where such interests
                are overridden by your interests or fundamental rights and freedoms.
              </li>
            </ul>
            <p>
              If you belong to one of the categories identified below, please expand the relevant
              section for further information. Please consider that you may belong to more than one
              category. If you need further assistance, please contact our Data Protection Officer
              via hello@devhance.co.
            </p>
          </div>

          <div>
            <h3>Recipients of personal data</h3>
            <p>
              All your personal data will be kept strictly confidential and will not be disclosed to
              unauthorized third parties or made public without your prior notification by this
              notice or in person.
            </p>
            <p>
              We may share your personal data with our technical partners that support Devhance in
              day-to-day operations and in provision of more convenient services to you. Our
              technical partners are located all over the globe, including countries with different
              data protection and privacy laws, including outside of the European Union legal
              system. In all cases, Devhance will not transfer your personal data internationally
              without reasonably ensuring that the rights and freedoms of you as the data subject
              are protected.
            </p>
            <p>
              We never sell your data or share it with other third parties in order to obtain direct
              or indirect financial gains.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div id="contacts-section-id" className="contacts-section">
      <Contacts />
    </div>
  </Layout>
);

export default TermsAndPrivacy;
