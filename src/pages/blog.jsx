import React, { Component } from 'react';
import { Link, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { Layout } from '../components';

const Post = ({ postData }) => (
  <div className="blog__post">
    <Link to={postData.fields.slug}>
      <Img
        fluid={postData.frontmatter.image.childImageSharp.fluid}
        alt={`Photo - ${postData.frontmatter.title}`}
        title={postData.frontmatter.title}
      />
      <h2 className="title">{postData.frontmatter.title}</h2>
      <p className="caption">{postData.frontmatter.description}</p>
      <div className="d-flex justify-content-between">
        <span>{postData.frontmatter.date}</span>
        <span>{postData.fields.readingTime.text}</span>
      </div>
    </Link>
  </div>
);

const QTY_POSTS = 5;
class BlogPage extends Component {
  state = {
    postsToShow: QTY_POSTS,
    posts: [],
    firstPost: {},
  };

  componentDidMount() {
    const posts = this.props.data.getAllOtherPosts.edges;
    const firstPost = this.props.data.getFirstPost.edges[0].node;

    this.setState({ posts, firstPost });
  }

  loadMorePosts = () => {
    const { postsToShow } = this.state;

    this.setState({ postsToShow: postsToShow + QTY_POSTS });
  };

  render() {
    const { firstPost, posts, postsToShow } = this.state;
    return (
      <Layout
        title="Blog"
        description="Are you interested in web design? Or you want to learn more about startups & entrepreneurship? Not enough motivation to start your business? Check our blog to find more."
        url={this.props.location.href}
      >
        <div className="blog__header__bg" />
        {firstPost.id && (
          <div className="blog__newest__post container">
            <div className="d-flex flex-wrap">
              <Img
                fluid={firstPost.frontmatter.image.childImageSharp.fluid}
                alt={`Photo - ${firstPost.frontmatter.title}`}
                title={firstPost.frontmatter.title}
                className="blog__newest__post__image"
              />
              <div className="blog__newest__post__label">The Newest Post</div>
              <div className="blog__newest__post__content">
                <div className="date">{firstPost.frontmatter.date}</div>
                <div className="title">{firstPost.frontmatter.title}</div>
                <Link to={firstPost.fields.slug} title={firstPost.frontmatter.title}>
                  <button className="btn btn--trasparent btn--white">Read More</button>
                </Link>
              </div>
              {/* <Post postData={firstPost} /> */}
            </div>
          </div>
        )}

        <div className="blog__posts">
          <div className="container">
            {posts.slice(0, postsToShow).map((post) => (
              <Post postData={post.node} key={post.node.id} />
            ))}

            {posts.length > postsToShow && (
              <div className="more" onClick={() => this.loadMorePosts()} role="button">
                <div>Load More</div>
                <img src="/images/svgs/blue-arrow-down.svg" alt="Load More" />
              </div>
            )}
          </div>
        </div>
      </Layout>
    );
  }
}

export default BlogPage;

export const pageQuery = graphql`
  query Blog {
    getFirstPost: allMarkdownRemark(
      limit: 1
      sort: { fields: frontmatter___date, order: DESC }
      filter: { frontmatter: { templateKey: { eq: "blog-post" } } }
    ) {
      edges {
        node {
          id
          fields {
            slug
            readingTime {
              text
            }
          }
          frontmatter {
            title
            description
            date(formatString: "MMMM DD, YYYY")
            image {
              childImageSharp {
                fluid(maxWidth: 1140, quality: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }

    getAllOtherPosts: allMarkdownRemark(
      skip: 1
      sort: { fields: frontmatter___date, order: DESC }
      filter: { frontmatter: { templateKey: { eq: "blog-post" } } }
    ) {
      edges {
        node {
          id
          fields {
            slug
            readingTime {
              text
            }
          }
          frontmatter {
            title
            description
            date(formatString: "MMMM DD, YYYY")
            image {
              childImageSharp {
                fluid(maxWidth: 600, quality: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
