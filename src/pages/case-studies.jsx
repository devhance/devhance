import React from 'react';
import { Link, graphql } from 'gatsby';
import Img from 'gatsby-image';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { Layout, Contacts } from '../components';

const CaseStudiesPage = ({ data, ...props }) => (
  <Layout
    title="Case studies reflect our love & passion"
    description="Find out more information about our successfully running projects from the library of the case studies."
    url={props.location.href}
  >
    <div className="first-screen-section first-screen-section--swap">
      <div className="container">
        <div className="first-screen-wrapper">
          <h1>Case Studies</h1>
          <h2>reflect our love & passion</h2>
          <AnchorLink href="#contacts-section-id">
            <button className="btn btn--trasparent btn--white">
              <strong>Build my product</strong>
              <span>
                <img src="/images/svgs/arrow.svg" alt="Arrow" />
              </span>
            </button>
          </AnchorLink>
          <div className="btn-scroll-down">
            <AnchorLink href="#case-studies-id" offset={() => 200}>
              <p>Scroll Down</p>
              <button>
                <img src="/images/svgs/scroll-down.svg" alt="Scroll Down" />
              </button>
            </AnchorLink>
          </div>
        </div>
      </div>
    </div>

    <div id="case-studies-id" className="case-studies-section">
      <div className="container">
        <div className="col-lg-12">
          <div className="section-title">Case Studies</div>

          {data.getAllCases.edges.map(({ node }, index) =>
            index % 2 ? (
              <div className="case row" key={index}>
                <div className="col-lg-6 col-md-6 case__image">
                  <Img
                    fluid={node.frontmatter.caseImage.childImageSharp.fluid}
                    alt={`Photo - ${node.frontmatter.cardTitle}`}
                    title={node.frontmatter.cardTitle}
                  />
                </div>

                <div className="col-lg-5 offset-lg-1 col-md-6 case__description">
                  <div className="title">{node.frontmatter.cardTitle}</div>
                  <p>{node.frontmatter.caseDescription}</p>
                  <Link to={node.fields.slug}>
                    <button className="btn btn--cyan--trasparent">View This Case</button>
                  </Link>
                </div>
              </div>
            ) : (
              <div className="case row" key={index}>
                <div className="col-lg-5 col-md-6 order-2 order-md-1 case__description">
                  <div className="title">{node.frontmatter.cardTitle}</div>
                  <p>{node.frontmatter.caseDescription}</p>
                  <Link to={node.fields.slug}>
                    <button className="btn btn--cyan--trasparent">View This Case</button>
                  </Link>
                </div>

                <div className="col-lg-6 offset-lg-1 col-md-6 order-1 order-md-2 case__image">
                  <Img
                    fluid={node.frontmatter.caseImage.childImageSharp.fluid}
                    alt={`Photo - ${node.frontmatter.cardTitle}`}
                    title={node.frontmatter.cardTitle}
                  />
                </div>
              </div>
            )
          )}
        </div>
      </div>
    </div>

    <div id="contacts-section-id" className="contacts-section">
      <Contacts />
    </div>
  </Layout>
);

export default CaseStudiesPage;

export const pageQuery = graphql`
  query CaseStudiesPage {
    getAllCases: allMarkdownRemark(
      sort: { fields: frontmatter___date, order: DESC }
      filter: {
        frontmatter: { templateKey: { eq: "case-page" } }
        fields: { slug: { ne: "/case-studies/harver/" } }
      }
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            cardTitle
            caseDescription
            caseImage {
              childImageSharp {
                fluid(maxWidth: 540, maxHeight: 480, quality: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
