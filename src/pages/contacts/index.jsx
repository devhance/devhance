import React from 'react';
import { Layout, Contacts } from '../../components';

const ContactsPage = (props) => (
  <Layout
    title="Contacts"
    description="Thank you for your interest in Devhance. Please, fill in the form below and a Devhance representative will contact you within the next 24 hours."
    url={props.location.href}
  >
    <div className="contacts-section contacts-section--sub">
      <Contacts />
    </div>
  </Layout>
);

export default ContactsPage;
