import React from 'react';
import { Layout } from '../../components';

const ContactThanks = () => (
  <Layout title="Thank you!" description="Thank you for your interest in Devhance!">
    <div className="not-found-content">
      <h1>Thank you for your interest in Devhance!</h1>
    </div>
  </Layout>
);

export default ContactThanks;
