import React from 'react';
import { Layout } from '../components';

const NotFoundPage = (props) => (
  <Layout
    title="Page Not Found"
    description="Sorry, but the page you were trying to view does not exist."
    url={props.location.href}
  >
    <div className="not-found-content">
      <h1>Page Not Found</h1>
      <h2>Sorry, but the page you were trying to view does not exist.</h2>
    </div>
  </Layout>
);

export default NotFoundPage;
