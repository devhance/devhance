import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import { Layout, Contacts } from '../components';

import imageAgileTeam from '../images/pages/about/agile-team.svg';
import imageEfficientWorkflow from '../images/pages/about/efficient-workflow.svg';
import imageCostEffectiveSolutions from '../images/pages/about/cost-effective-solutions.svg';
import imageCustomerCentricApproach from '../images/pages/about/customer-centric.svg';

const AboutUsPage = (props) => (
  <Layout
    title="Bringing value for web owners"
    description="At Devhance we are working on changing the web industry with new approach for creating super fast websites & web apps for business owners."
    url={props.location.href}
  >
    <FirstScreenSection />
    <NumbersSection />
    <WhySection />
    <ContactsSection />
  </Layout>
);

const FirstScreenSection = () => (
  <div className="first-screen-section first-screen-section--swap">
    <div className="container">
      <div className="first-screen-wrapper">
        <h1>
          Bringing value <br /> for web owners
        </h1>
        <h2>at Devhance we are working on changing the web industry</h2>
        <AnchorLink href="#contacts-id">
          <button className="btn btn--trasparent btn--white" type="button">
            <strong>Build my product</strong>
            <span>
              <img src="/images/svgs/arrow.svg" alt="Arrow" />
            </span>
          </button>
        </AnchorLink>
        <div className="btn-scroll-down">
          <AnchorLink href="#why-devhance" offset={() => 200}>
            <p>Scroll Down</p>
            <button type="button">
              <img src="/images/svgs/scroll-down.svg" alt="Scroll Down" />
            </button>
          </AnchorLink>
        </div>
      </div>
    </div>
  </div>
);

const NumbersSection = () => (
  <div className="numbers-section">
    <div className="container">
      <div className="row">
        <div className="item col-lg-4 col-md-6">
          <span className="number">20+</span>
          <span className="text">
            successfully running JAMstack <br /> websites & web apps
          </span>
        </div>
        <div className="item col-lg-4 col-md-6">
          <span className="number">30k</span>
          <span className="text">
            hours of productive team work
            <br /> & delivering excellence
          </span>
        </div>
        <div className="item col-lg-4 col-md-6">
          <span className="number">60+</span>
          <span className="text">
            zoom calls with clients every month to bring top-notch product{' '}
          </span>
        </div>
        {/*<div className="item col-lg-3 col-md-6">
          <span className="number">20+</span>
          <span className="text">cooperation with different clients</span>
        </div>
        */}
      </div>
    </div>
  </div>
);

const WhySection = () => (
  <div id="why-devhance" className="why-section">
    <div className="container">
      <div className="wrapper">
        <div className="why-section__heading">
          <h3 className="section-title section-title--small">Why Devhance?</h3>
          <p>
            We respond to your emails, finish the job and care about making things simpler for every
            business owner. A lot of our customers complain about companies, agencies that are not
            exceed the expectations. Devhance’s team is here to change your mind.
          </p>
        </div>

        <div className="row">
          <div className="col-md-6">
            <div className="item">
              <img src={imageAgileTeam} alt="Agile team" />
              <h4>Agile team</h4>
              <p>
                No more separate developers or unresponsive agencies who wouldn’t work like a
                structured team. We are a cross-functional group of experienced individuals who
                analyze, design, build, test, and deliver an increment of value in a short time.
              </p>
            </div>
          </div>
          <div className="col-md-6">
            <div className="item">
              <img src={imageCustomerCentricApproach} alt="Customer-centric approach" />
              <h4>Customer-centric approach</h4>
              <p>
                We do market analysis before providing the wireframes when other companies don’t
                even know your competitors. Every functionality during development stage is
                discussed in details and our team always advise on the further enhancements.
              </p>
            </div>
          </div>
          <div className="col-md-6">
            <div className="item">
              <img src={imageCostEffectiveSolutions} alt="Cost-effective solutions" />
              <h4>Cost-effective solutions</h4>
              <p>
                Our mission is not only to deliver high-quality product - we are concentrated on
                offering the cost-effective solutions to decrease your monthly expenses on
                maintenance, hosting, server, plugins and other extra services that are slowing you
                down.
              </p>
            </div>
          </div>
          <div className="col-md-6">
            <div className="item">
              <img src={imageEfficientWorkflow} alt="Efficient workflow" />
              <h4>Efficient workflow</h4>
              <p>
                You don’t need to beg us for scheduling meetings. Our team guarantees a proactive
                communication with reports about current progress. Working as a small team helps us
                to meet deadlines and make customer experience as good as it can be.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

const ContactsSection = () => (
  <div id="contacts-id" className="contacts-section">
    <Contacts />
  </div>
);

export default AboutUsPage;
