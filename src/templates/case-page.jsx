import React from 'react';
import { graphql } from 'gatsby';
import Img from 'gatsby-image';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { Layout, Contacts } from '../components';

const CasePageTemplate = ({ pageData, ...props }) => (
  <Layout
    title={pageData.seo.title}
    description={pageData.seo.description}
    url={props.location.href}
  >
    <div className="case case__header case__header--myvisiticeland">
      <div className="container">
        <div className="first-screen-wrapper">
          <h1>{pageData.pageTitle}</h1>
          <div className="btn-scroll-down">
            <AnchorLink href="#case-overview-id">
              <p>Scroll Down</p>
              <button>
                <img src="/images/svgs/scroll-down.svg" alt="Scroll Down" />
              </button>
            </AnchorLink>
          </div>
        </div>
      </div>
    </div>

    <div id="case-overview-id" className="case case__overview">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 description-block">
            <img
              className="section-case-logo"
              src={pageData.caseLogo.publicURL}
              alt={`Logo - ${pageData.cardTitle}`}
              title={pageData.pageTitle}
            />

            <h3 className="section-name">Overview</h3>
            <p className="section-overview section-overview--padding">{pageData.overview.text}</p>
            <a
              className="section-link"
              target="_blank"
              href={pageData.caseLink}
              rel="noopener noreferrer"
            >
              {pageData.caseLinkName}
            </a>
          </div>
          <div className="col-lg-7 section-image">
            <Img
              fluid={pageData.overview.image.childImageSharp.fluid}
              alt={`Overview - ${pageData.pageTitle}`}
              title={pageData.pageTitle}
            />
          </div>
        </div>
      </div>
    </div>

    <div className="case case__challenge">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 description-block">
            <h3 className="section-name section-name--white">Challenge</h3>
            <p className="section-overview section-overview--white">{pageData.challenge.text}</p>
            <ul className="section-list">
              {pageData.challenge.list.map((item, index) => (
                <li className="d-flex justify-content-start" key={index}>
                  <img src="/images/svgs/checkmark.svg" alt="Checkmark" />
                  {item.text}
                </li>
              ))}
            </ul>
          </div>
          <div className="col-lg-7 section-image">
            {/* <Img
              fluid={pageData.challenge.image.childImageSharp.fluid}
              alt={`Challenge - ${pageData.pageTitle}`}
              title={pageData.pageTitle}
            /> */}
          </div>
        </div>
      </div>
    </div>

    <div className="case case__solution">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 description-block">
            <h3 className="section-name">Solution</h3>
            <p className="section-overview">{pageData.solution.text}</p>
            <ul className="section-list">
              {pageData.solution.list.map((item, index) => (
                <li className="d-flex justify-content-start align-items-start" key={index}>
                  <img src="/images/svgs/line-list.svg" alt="Line list" />
                  {item.text}
                </li>
              ))}
            </ul>
          </div>

          <div className="col-lg-7 section-image">
            <Img
              fluid={pageData.solution.image.childImageSharp.fluid}
              alt={`Solution - ${pageData.pageTitle}`}
              title={pageData.pageTitle}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-lg-6 offset-lg-3 review-block">
            <h4>What client says about us</h4>
            <p>{pageData.review.text}</p>
            <div className="review-author">
              <strong>{pageData.review.author}</strong>
              <span>{pageData.review.description}</span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="contacts-section-id" className="contacts-section">
      <Contacts />
    </div>
  </Layout>
);

const CasePage = ({ data, ...props }) => {
  const { markdownRemark } = data;

  return <CasePageTemplate pageData={markdownRemark.frontmatter} {...props} />;
};

export default CasePage;

export const pageQuery = graphql`
  query CasePage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        cardTitle
        pageTitle
        caseDescription
        caseLink
        caseLinkName
        caseLogo {
          publicURL
        }
        overview {
          text
          image {
            childImageSharp {
              fluid(quality: 90) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        challenge {
          text
          list {
            text
          }
          image {
            childImageSharp {
              fluid(quality: 95) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        solution {
          text
          list {
            text
          }
          image {
            childImageSharp {
              fluid(quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        review {
          text
          author
          description
        }
        seo {
          title
          description
        }
      }
    }
  }
`;
