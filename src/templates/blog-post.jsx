import React from 'react';
import { Link, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { Layout } from '../components';

const BlogPostTemplate = ({ postData, postAlso, ...props }) => (
  <Layout
    title={postData.frontmatter.seo.title}
    description={postData.frontmatter.seo.description}
    image={postData.frontmatter.image.childImageSharp.fluid.src}
    url={props.location.href}
    postNode={postData.frontmatter}
    postSEO
  >
    <div itemScope itemType="http://schema.org/Article">
      <div className="blog-post__header">
        <Img
          className="blog-post__header__bg"
          fluid={postData.frontmatter.bgImage.childImageSharp.fluid}
          alt={`Photo - ${postData.frontmatter.seo.title}`}
          title={`${postData.frontmatter.seo.title} – Devhance`}
        />
        <div className="blog-post__header__content">
          <div className="date">{postData.frontmatter.date}</div>
          <h1 className="title">{postData.frontmatter.title}</h1>
          <span className="time">{postData.fields.readingTime.text}</span>
        </div>
      </div>
      <article className="blog-post__content container">
        <div itemProp="articleBody" dangerouslySetInnerHTML={{ __html: postData.html }} />
      </article>

      <div className="blog-post__also-like container">
        <div className="title">You May Also Like:</div>
        <div className="row">
          {postAlso.map((post) => (
            <div className="col-md-4 blog-card" key={post.node.id}>
              <Link to={post.node.fields.slug}>
                <Img
                  fluid={post.node.frontmatter.image.childImageSharp.fluid}
                  alt={`Photo - ${post.node.frontmatter.title}`}
                  title={`${post.node.frontmatter.title} – Devhance`}
                />
                <span className="blog-card__date">{post.node.frontmatter.date}</span>
                <h4 className="blog-card__title">{post.node.frontmatter.title}</h4>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  </Layout>
);

const BlogPost = ({ data, ...props }) => {
  const { markdownRemark, allMarkdownRemark } = data;

  return (
    <BlogPostTemplate postData={markdownRemark} postAlso={allMarkdownRemark.edges} {...props} />
  );
};

export default BlogPost;

export const pageQuery = graphql`
  query BlogPost($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
        readingTime {
          text
        }
      }
      frontmatter {
        title
        image {
          childImageSharp {
            fluid(maxWidth: 790, quality: 100) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        bgImage {
          childImageSharp {
            fluid(maxWidth: 1920, quality: 100) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        date(formatString: "MMMM DD, YYYY")
        seo {
          title
          description
        }
      }
    }

    allMarkdownRemark(
      limit: 3
      sort: { fields: frontmatter___date, order: DESC }
      filter: { id: { ne: $id }, frontmatter: { templateKey: { eq: "blog-post" } } }
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
            image {
              childImageSharp {
                fluid(maxWidth: 350, maxHeight: 310, quality: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
