---
templateKey: blog-post
title: Where is graphic design used?
description: >-
  People do not fully understand where is graphic design used and what differs
  it from web design. It doesn’t matter if you are a business owner or a project
  manager, our mission is to explain you what does it mean.
image: /images/cms/“where-graphic-design-is-used”.jpg
bgImage: /images/cms/blog-bg-2.jpg
date: 2019-10-24T12:52:48.895Z
seo:
  title: Where is graphic design used?
  description: >-
    People do not fully understand where is graphic design used and what differs
    it from web design. It doesn’t matter if you are a business owner or a
    project manager, our mission is to explain you what does it mean.
---
People do not fully understand where is graphic design used and what differs it from web design. It doesn’t matter if you are a business owner or a project manager, our mission is to explain you what does it mean.

## What does graphic design mean and where we use it?

Devhance’s team would say that it’s a kind of communication between your business and your customers through the use of type and image. It is also known as visual communication.

Common uses of graphic design include identity (logos and branding), publications (magazines, newspapers and books), print advertisements, posters, billboards, website graphics and elements, signs and product packaging. So if you notice uncommon icons, logos or not typical illustrations somewhere, this work was done by graphic designer.

## What is the main difference between web & graphic design?

Web design and graphic design looks an irrelevant group, but people still often get confused. It’s really hard to explain what does web design mean. The different areas of web design include web graphic design; interface design; authoring and user experience design.

So what is the main difference between web & graphic design? Web designers create a design from scratch for the websites, when graphic designers create separate elements for the web designers who use them for creating a web design. Definitely we live in the era of highly skilled professionals and some of graphic designers are not only creating logos, they also create design for full websites. But let’s be honest, you can’t be an expert in everything.

Also it’s important to mention, that a lot of people do not see the difference between web designers, web developers and graphic designers. Generally, the layman always calls web designers and graphic designers as “art designer”. Because all their work are misunderstood by the involvement of art skill and creative ability. Actually this is the big problem in the design industry.



If you learn more about graphic or web design, follow us on: [Linkedin](https://www.linkedin.com/company/devhance) & [Facebook](https://www.facebook.com/devhance)
