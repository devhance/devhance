---
templateKey: blog-post
title: TOP 5 BUSINESS IDEAS TO TRY IN 2019
description: >-
  You want to start your business, but don’t know how? The “laptop lifestyle”
  allows you to create an income and manage your company with your phone. Here
  is our top 5 business ideas you should try in the second half of 2019.
image: /images/cms/top-7-business-ideas-to-build-in-2019.jpg
bgImage: /images/cms/blog-bg-3.jpg
date: 2019-09-21T10:16:58.077Z
seo:
  title: TOP 5 BUSINESS IDEAS TO TRY IN 2019
  description: >-
    You want to start your business, but don’t know how? The “laptop lifestyle”
    allows you to create an income and manage your company with your phone. Here
    is our top 5 business ideas you should try in the second half of 2019.
---
You want to start your business, but don’t know how? The “laptop lifestyle” allows you to create an income and manage your company with your phone. Here is our top 5 business ideas you should try in the second half of 2019.

## Ghostwriting

We don’t know the company which is not willing to pay for good SEO or compelling articles. According to forbes.com, most content writers charge 10 cents per word, so a 3000 word article would garner you $300. You can secure ghostwriting gigs from platforms like [Freelancer](https://www.freelancer.com), [Contena](https://www.contena.co/) and more. You can do it as side hustle also, not a full-time job. What you just need is patience and eye-catching portfolio which will help you to build your personal brand.

## eBook

Did you manage to get 500k organic followers on Instagram? Do you make delicious and amazing cakes, but nobody knows about it? Just share your experience by creating an eBook! Self publishing can be a very successful venture if you are disciplined to pump out a lot of content. 60-70 pages of useful content will be more than enough.

## Youtube channel

The most underrated source if income is youtube. Several successful Youtubers earn millions by posting videos. However, an average Youtuber earns around $3 to $5 per 1000 views. But you need to think what might be interesting for people. Start with learning Youtube guidelines and how to use its tools. Make an investment in a solid camera with microphone and start right away!

## Vending machines

Nobody mentioned vending machines, but we think that it is still a good way to earn money. Buy 1-2 vending machine(s) for 1,000-2,000$ each and put them into public places. Make sure that price for rent is not fixed and depends on number of items sold. Invest profit into buying more vending machines. Enjoy your life!

## Online workshops/classes

Online courses are fabulous, but online workshops are getting really popular. Access to you for a block of time is really valuable to a workshop attendee. Offering a specific time to be online forces people to actually show up and take in what you’re teaching as opposed to an evergreen course that can be taken at any time.

Follow us on: [Linkedin](https://www.linkedin.com/company/devhance/), [Facebook](https://www.facebook.com/devhance/)
