---
templateKey: blog-post
title: 5 proven ways how to stop being lazy
description: >-
  Are you lazy? Don’t lie, everyone is lazy sometimes. How laziness affects you?
  It’s quite interesting question to discuss and here are 5 proven ways to stop
  being lazy.
image: /images/cms/“how-laziness-affects-you”.png
bgImage: /images/cms/cover-for-blog-1.png
date: 2020-06-09T10:33:13.735Z
seo:
  title: 5 proven ways how to stop being lazy
  description: >-
    Are you lazy? Don’t lie, everyone is lazy sometimes. How laziness affects
    you? It’s quite interesting question to discuss and here are 5 proven ways
    to stop being lazy.
---
## Remind yourself about bad times

All of us faced the bad times in the past. Not everyone grew up in a rich family, but rich also cry.

So, when you have zero motivation to do anything, just remind yourself about bad times that you’ve experienced in your life. It’s kind of a questionable advice despite the fact that it really helps.

It can be quarrels with your parents or friends, thing that you were dreamed of and couldn’t afford. The substantial point here is to cause internal aggression and to start doing something.

## Get out of your comfort zone

Lack of a motivation? Deprive yourself all the necessities. When we put ourselves into the place where we are limited with things that we used to have in the past, our mind is trying to find the ways to bring the comfort back.

Ask the questions other people don’t like to. This means having frequent and open conversations with your family, friends or colleagues. You should ask tough questions to find real issues and get a much needed support.

## Shut down the escape routes

Just ask yourself: where do I usually escape to instead of doing my work? Your smartphone or laptop? Instagram or twitter?

Eliminate distractions. By setting up that small physical obstacle, like switching a silent mode on your phone that will help you to avoid the distraction’s trap. Find the ways to make your “escape routes” less accessible: use an extension to block sites that disturb you; find a quite place for your daily work and etc.

## Nothing is perfect

Perfectionism is a thing that everyone wants to get closer to, but no one couldn’t.

\- “If you feel like you aren’t growing, think about where you were a year ago.”

Researchers from North-Western University of Chicago noted that young people are facing more competitive environments, more unrealistic expectations, and more anxious and controlling parents than generations before. This rise in perfectionism is causing people to be overly critical of themselves and others. It’s also led to an increase in depression and anxiety, so the best way to avoid it – forget about perfectionism term!

## Stop worrying about someone’s opinion

Keep things in perspective. If everyone knew how little others think about them. Everyone has enough to occupy their mind. They also have their own insecurities. If you're worried about how you come across to someone you’ve just met, keep in mind that they’re probably doing the same. 

Allow yourself to be vulnerable. It can be terrifying to go against the grain, speak out, take a risk, or face disapproval. But decide what matters to you, trust yourself, and go for it. We don’t grow by always playing it safe; we grow by allowing ourselves a chance to fail. 



If you like this post, follow us on: [LinkedIn](https://www.linkedin.com/company/devhance/), [Facebook](https://www.facebook.com/devhance/), [Twitter](https://twitter.com/devhance_co) & [Behance](https://www.behance.net/devhance)!
