---
templateKey: blog-post
title: 5 tips how to start this year strong
description: >-
  We hope you’ve already started your year strong. Despite the fact that every
  year has its own ups and downs, Devhance’s team prepared 5 good tips how you
  can be stronger than ever.
image: /images/cms/“7-tips-how-to-start-the-year-strong”.png
bgImage: /images/cms/cover-for-blog-2.png
date: 2020-01-29T09:44:20.989Z
seo:
  title: 5 tips how to start this year strong
  description: >-
    We hope you’ve already started your year strong. Despite the fact that every
    year has its own ups and downs, Devhance’s team prepared 5 good tips how you
    can be stronger than ever.
---
## Tip #1 - Planning

Plan your year, plan your months, plan your days. It’s never too late even if January is almost gone. According to entrepreneur.com, it’s really important to plan at the same time everyday, so after 2-3 weeks you will create a habit. Refer back to your list as often as you can and after few month your productivity level will go up.

## Tip #2 – Do what you love

Do what you love, not what you like. We need to pay bills, support our families and friends. There are hundreds of things we should do, that we neither like nor love, nor have to, in the strictest sense. And you definitely should cut back on the things you like to make room for what you love, you should also cut back on the shoulds. If something isn’t a must and it doesn’t fill you with excitement, then you might as well abandon the guilt about avoiding it now.

## Tip #3 – Health & Gratitude first

Let’s focus on living more healthful life. Forget about junk food, say “NO” to refined sugar. Get plenty of regular sleep. Don’t forget to stay active – do the morning gymnastics, walk more, keep your body in shape. Do the tough, less enjoyable task first and bundle it with joy by then following it up with an enjoyable, more pleasurable task. Practice consciously being grateful; take some time to reflect on what you are grateful for each day or each week. In just 1 month we guarantee that you will be unrecognizable.

## Tip #4 – Your goal is 1 year closer

Just ask yourself, am I closer to my goal that I wrote on the paper 3 years ago? No? You must work harder. Life is simple but we make it unbelievably complicated. If every person on the Earth was fully committed to the goals we set, everyone will be successful. Remember why you started and be grateful for everyone who gave you a piece of advice or supported you in the hard times.

## Tip #5 – Get rid of “bad” environment

The first quarter of the year is also a great time to attend to your networking efforts. A large part of this is to update and clean your networking database, which is basically the list of all your networking contacts. Which contacts have you not kept touch with in more than six months? Which contacts may you need to remove from your networking database? Think about it and do progress.



If you want to learn more life-changing tips, follow us on: [LinkedIn](https://www.linkedin.com/company/devhance/) & [Facebook](https://www.facebook.com/devhance)
