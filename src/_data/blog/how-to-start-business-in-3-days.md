---
templateKey: blog-post
title: How to start business in 3 days
description: >-
  When procrastination takes over our mind and you can’t be fully committed to
  the goals, it’s so important to find out the way to eliminate all the
  distractions and start acting fast. Devhance’s team wants to share 3 tips how
  to start business in 3 days and why it is worth it.
image: /images/cms/“how-to-start-a-business-in-2-days“.png
bgImage: /images/cms/cover-for-blog-3.png
date: 2020-02-05T11:57:13.945Z
seo:
  title: How to start business in 3 days
  description: >-
    When procrastination takes over our mind and you can’t be fully committed to
    the goals, it’s so important to find out the way to eliminate all the
    distractions and start acting fast. Devhance’s team wants to share 3 tips
    how to start business in 3 days and why it is worth it.
---
When procrastination takes over our mind and you can’t be fully committed to the goals, it’s so important to find out the way to eliminate all the distractions and start acting fast. Devhance’s team wants to share 3 tips how to start business in 3 days and why it is worth it.

## Start networking with your circle

Tell your friends, parents, their friends that you’ve started doing something. You need to do sales first. Even if you don’t have a website or a cool logo, you must do sales. 

Think about pros and cons of your products and see if you can really sell them right now. Be wary of investments, promises, equipment and staff – that might not be necessary during the startup phase.

Acting fast is a key to success, so don’t waste your time and start right now.

## Start giving for free

Build a little community of those you love and who love you – and then you should offer your services for free – at least at first. Eventually you’ll have a growing circle of people who have seen your work and who believe you. That’s a kind of connections using “giving for free” method you’re looking to create if you’re going to start a business in 2020.

We truly believe that this suggestion will help you not only to do sales but also to meet new people. The law of probability ensures that the more new people you know, the more opportunities will come your way in closest future.

## Be useful for people and customers will come

Today everything starts with educational/entertaining content on your social media. Start giving value and customers will come.

Test your ideas on your friends. They need to vote with their cash, with real money. Launch an educational course, offer a 30% discount and you will see how many friends will buy and what they can say about the product. That’s the only real way how you can test fast and make the right decision. Don’t be afraid of hearing someone’s opinion, you will gain a huge experience of how your audience looks like and what is the right thing to sell.



If you want to learn more about our company, follow us on: [LinkedIn](https://www.linkedin.com/company/18723408) & [Facebook](https://www.facebook.com/devhance/)
