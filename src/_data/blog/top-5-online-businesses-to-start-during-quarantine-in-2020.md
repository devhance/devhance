---
templateKey: blog-post
title: TOP 5 online businesses to start during quarantine in 2020
description: >-
  Are you stuck on quarantine? Or you are enjoying working from home? It doesn’t
  matter where you are or what you are doing right now, here are our top 5
  online businesses to start during quarantine in 2020.
image: /images/cms/“top-5-online-business-to-build-in-2020”.png
bgImage: /images/cms/cover-for-blog-3.png
date: 2020-04-13T09:42:12.963Z
seo:
  title: TOP 5 online businesses to start during quarantine in 2020
  description: >-
    Are you stuck on quarantine? Or you are enjoying working from home? It
    doesn’t matter where you are or what you are doing right now, here are our
    top 5 online businesses to start during quarantine in 2020.
---
Are you stuck on quarantine? Or you are enjoying working from home? It doesn’t matter where you are or what you are doing right now, here are our top 5 online businesses to start during quarantine in 2020.

## SAAS - Software As A Service

The world is rapidly growing and more SAAS solutions show up. We would say that it is not the easiest way of online business, but it definitely can be life-changing.

There are a huge amount of ideas can be implemented:

* starting with simple to-do list and expanding it to the habits reminder platform with subscription model;
* property management platform that helps to gather all the leasing documents and manage your property from a mobile phone; 
* event management platform – how to control event planners with project managers in once place.
* faster and smarter collaboration tool for entrepreneurs as video chat platform(not as primitive as video conferences);

We don’t know what kind of software might have a bright future in the end, so the best advice is to not be scared of doing experiments.

## Start a dropshipping store

The algorithm is pretty easy to understand even if you’ve never been to online store business. You sell an item, a customer places an order for that item, the supplier ships the product to the customer, and you handle the customer service part of the transaction.

So what is the advantage of having a dropshipping model?

You don’t need an inventory or any kind of warehouse! Just imagine how many problems are eliminated. But don’t forget about website, positioning, payments and providing excellent customer service.

## Selling educational digital products

U.S. Education Market Will Reach USD 2,040 billion by 2026 according to globenewswire.com. So it’s the best time to think about creating and selling your own educational digital products.

If you consider yourself an expert on a particular topic, digital products are a great way to package that information and sell it to others looking to learn. People are selling digital templates & tools, licenses to digital assets and many more things. You can use apps like “Digital Downloads”, “FetchApp”, “SkyPilot”, “Sendowl” and others to sell your knowledge.

No more risk selling physical products that might not be bought in a year. Just start today, be creative, build an audience and don’t sell crap.

## Travel consulting

We don’t know people who are not passionate about traveling. People waste a lot of time to stay updated about things like best ticket options and cheap hotel deals. So, why you can’t help them?

You can start with personal page on Instagram, Facebook and LinkedIn OR you can gather a bunch of travelers who are experienced enough and to grow your freelance travel consulting business. Also don’t forget about your family and friends, they can help you earn free word of mouth publicity.

Networking here is a key, collaborating with travel agencies and influencers can open new opportunities and increase your business value.

And remember - there is a lack of travel consultants online! You can search on google and see how many requests there will be comparing to accountant, salesperson or designer.

## YouTube channel

It’s not about vlogging and making thousands of dollars, it’s all about giving value and promoting your personal brand. 

We see more and more real estate agents, entrepreneurs and company owners start their personal YouTube channel. Investing in a camera, microphone to make quality videos is not risky anymore. Creating a content plan and to speak on camera is much more complicated, but nobody says that there are no difficulties here.

Start right now, don’t waste your time and keep going until you won’t be satisfied.



If you want to learn more about online business, follow us on: [LinkedIn](https://www.linkedin.com/company/devhance/) & [Facebook](https://www.facebook.com/devhance)
