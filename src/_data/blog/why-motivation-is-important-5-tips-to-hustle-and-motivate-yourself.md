---
templateKey: blog-post
title: Why motivation is important?
description: >-
  Life is very basic, we make it unbelievably complicated. People lack the
  ability to put their life in perspective, they don't want to decide what are
  next steps in their lives. We call it “lack of motivation”.
image: /images/cms/post-why-motivation-is-important.jpg
bgImage: /images/cms/blog-bg-3.jpg
date: 2019-04-08T15:00:00.000Z
seo:
  title: Why motivation is important?
  description: >-
    Life is very basic, we make it unbelievably complicated. People lack the
    ability to put their life in perspective, they don't want to decide what are
    next steps in their lives. We call it “lack of motivation”.
---
Life is very basic, we make it unbelievably complicated. People lack the ability to put their life in perspective, they don't want to decide what are next steps in their lives. We call it “lack of motivation”.

We think, that motivation is a kind of life skill that comes from your mind after you have some kind of limitations in our life. Do you hear about theory, that people, who got everything when they were young, will never be motivated enough to really reach something? Nobody gives a f\*ck about earning money or pushing ass to the top when you’ve got a sport car, unlimited amount of money and huge house with a breathtaking view on the mountains. That’s true. But when you have some kind of limitations in terms of food, entertainment or any sh\*t we can’t spend a lot of money on – the motivation comes.

STOP complaining and create a shortage of something. Even if you are so lazy and you think, that you’ve got everything, make yourself to feel a lack of something – it can be whatever you want.

STOP letting other people's opinions control you! Are you still taking care of people, who judge you by the posts on social medias?

GET RID OF people who are not believing in you, who are just time and money wasters. Surround yourself with the dreamers and the doers, believe us, it really helps.

REMEMBER, that MOTIVATION CLARIFIES A GOAL. Once you understand what is the goal, motivation will help you to priorities your life.

FIND OUT how many days you have until you will be 60 years old. If you are 30 now, sorry, but you’ve just wasted a half of your life doing nothing. And nobody will remember how many hours you spent watching TV shows or working on somebody’s DREAM startups. You will never be a “game changer” or influencer in this world without creating any kind of motivation to achieve goals.
