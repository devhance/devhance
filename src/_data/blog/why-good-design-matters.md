---
templateKey: blog-post
title: Why good design matters?
description: >-
  The most powerful means of communication today is design. You’re still
  guessing why it is so important for your business? To outline exactly why good
  design matters, our team created the list of proven reasons.
image: /images/cms/how-to-deliver-top-notch-design.jpg
bgImage: /images/cms/blog-bg-2.jpg
date: 2019-09-17T13:55:19.643Z
seo:
  title: Why good design matters?
  description: >-
    The most powerful means of communication today is design. You’re still
    guessing why it is so important for your business? To outline exactly why
    good design matters, our team created the list of proven reasons.
---
The most powerful means of communication today is design. You’re still guessing why it is so important for your business? To outline exactly why good design matters, our team created the list of proven reasons.

## First impression matters

According to [problogger.com](https://problogger.com/net-users-take-120th-of-a-second-to-judge-your-blogs-design/), net users take 1/20th of a second to judge your blog’s design. It proves the fact that designers have to make a good impression within the space of 45 miliseconds. Even walking through a supermarket, reviewing catalogue or researching online, you encounter hundreds of brands, businesses - they all competing to get our attention and sell their products. One more proof: if you are a web agency or IT company, you know that customers always asking for having a WOW effect on a home page when user enters a website. Figure out what key things have WOW effect or can be memorable, so it will help you to boost your brand awareness.

## Well-designed experience = more revenue

Design leads to profit, or so studies would suggest. One of the most conclusive studies ever published on how design impacts profit is that of the Design Council in 2005 which examined the portfolios of 63 companies traded on the FTSE through a period of 10 years, between 1995-2004. The study culminated the evidence that the companies emphasizing design outperformed the FTSE 100 index by 200% during the span of the study. Money demands attention, therefore, design demands attention. It’s not only about making your content look professional, consistent and enticing, but the reality of it goes so much deeper than that. Design well, make money.

## Broadens your target audience

You might ask how it broadens your target audience? Making simple changes in the design can help your business to position a brand to a whole new category of people. Imagine that you owned a business that marketed a product that was predominantly targeted to women, but you really wanted to tap into a male market. Mixing up the design of your packaging to appeal to a more masculine demographic could be key to transitioning your brand. So think about where your brand could extend to with just a few stylistic adjustments. And do not stop making experiments, they lead to success.

## Good design solution doesn't cost too much

“I don’t want to pay 30,000$ for a logo!”. Definitely you don’t have to.  Knowing that you're never going to be perfect, but striving for incremental improvements is a good sign for successful business. Make sure that you hire a good team who has a strong understanding of your customer and craft a message that will resonate. But not investing in design can cause huge problems!  A more layman-friendly figure that this study by The Design Council also determined was that “every £100 a design alert business spends on design increases turnover by £225”. So, investing in design is a bit like investing in future profits, it may cost a bit now, but a little down the track, these studies suggest that you’ll likely be glad you did so.



Follow us on: [Linkedin](https://www.linkedin.com/company/devhance/), [Facebook](https://www.facebook.com/devhance/)
