---
templateKey: blog-post
title: How colors affect your visitors?
description: >-
  Some companies do not fully understand how color palette affects their
  visitors. Nobody wants to do a research to find the most impactful colors for
  a marketing company. Have you ever thought why giants like Facebook or
  Microsoft didn’t choose purple or black colors for their logos? Success hides
  in the details.
image: /images/cms/post-how-colors-affect-your-visitors.jpg
bgImage: /images/cms/blog-bg-2.jpg
date: 2019-06-10T15:00:48.335Z
seo:
  title: How colors affect your visitors?
  description: >-
    Some companies do not fully understand how color palette affects their
    visitors. Nobody wants to do a research to find the most impactful colors
    for a marketing company. Have you ever thought why giants like Facebook or
    Microsoft didn’t choose purple or black colors for their logos? Success
    hides in the details.
---
Some companies do not fully understand how color palette affects their visitors. Nobody wants to do a research to find the most impactful colors for a marketing company. Have you ever thought why giants like Facebook or Microsoft didn’t choose purple or black colors for their logos? Success hides in the details.

## Black & White colors

Colors that we use the most, we can call them “universal colors”. These 2 colors can be used as backgrounds for almost every purpose: starting from landing pages and ending with e-commerce websites. Creating CTA elements using black and white colors might not be a good idea (for CTA we should use bright accent colors that attract attention).

## Blue

For everyone who didn’t notice, we use blue color as main color at [Devhance](/). The color is associated with tranquility and composure. We would say that this color is about the trust. 3rd most used color after black and white. Unfortunately, blue color in lighter and darker shades may not fit for every purpose. Marketing experts mention, if you use blue color in foodstuffs companies, it will cause problems, because this color is not a natural color for food.

## Red

The red color is definitely a CTA color. It increases the heartbeat and causes faster breathing. It is an intense and powerful color that is associated with demand and aggression. Red is also known to stimulate our appetite. Use it properly, the red color is not an easy color to combine with.

## Green

Organic products, nature and ecology. Do not use green for selling electronic devices – it will not work! Also, green color tells us about growth and development. Use it for your brand if you want to show clients aesthetics and purity.

## Purple

Purple is all about wealth and royalty. According to kick metrics this color is not usable for CTA elements, but it’s ideal for premium products. Chocolate “Milka” is a good example of using purple color.

## Yellow

The yellow color is the attention grabber. It screams to us: “Look at me!”, “Click me!”. However, yellow is harshest on the eyes and therefore stimulates our emotional side. It might be the best example for uplifting user experience and enhance happiness.
