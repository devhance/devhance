---
templateKey: blog-post
title: 5 Top web design mistakes
description: >-
  After 3 years working with a huge amount of web designers, we would love to
  share our top 5 web design mistakes and how to avoid them.
image: /images/cms/post-5-top-web-design-mistakes.jpg
bgImage: /images/cms/blog-bg-2.jpg
date: 2019-04-15T15:30:24.419Z
seo:
  title: 5 Top web design mistakes
  description: >-
    After 3 years working with a huge amount of web designers, we would love to
    share our top 5 web design mistakes and how to avoid them.
---
After 3 years working with a huge amount of web designers, we would love to share our top 5 web design mistakes and how to avoid them.

5 top web design mistakes and how to avoid them:

## Not understanding your audience

1 week ago we noticed really awesome works done by a web designer on Instagram. Dark concept with medium green accent color is a perfect idea. But that was a project for kids and their parents to learn abc – that is the main point. Our mission is to show colorful world for our children, not dark and evil.

## Avoiding mobile-friendly design

78.8% of our internet traffic comes from mobile devices. It doesn’t matter if you are a freelance web designer or you are working with a team of developers, you MUST provide mockups for extra-small screens, otherwise your web design concept will be crushed by developers while converting it into mobile design.

## Too many fonts

Stick to 2-3 fonts maximum designing your website. Surfing the internet there is a huge amount of websites that overusing fonts. Remember that it looks unprofessional and disorganized.

## Hard-to-read content

We’ve seen a lot of web designer’s works recently. But even perfect concept has a mistake of hard-to-read content. Especially you can see it on main/home screens. Do not ignore text shadow if it’s needed. Also you can change text’s color shade or separate the text with adding effects to the background on exact place.

## Ignoring the grid

The most common mistake creating web design in 2019. If you still don’t use grid today, you might be in trouble. No doubts, there are projects that need creativity, uncommon solutions. But most of pages follow the concept of creating standard sections with icons, images, CTA buttons and text information. Make sure that you know what does grid mean and why it is important for developers to make website fully responsive.
