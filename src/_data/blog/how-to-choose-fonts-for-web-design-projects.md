---
templateKey: blog-post
title: How to choose fonts for web design projects?
description: >-
  Choosing a font for your project is not an easy thing to do. Finding a perfect
  match, defining your audience and understanding the purpose are the most
  important things you need to have an eye for.
image: /images/cms/choosing-fonts.jpg
bgImage: /images/cms/blog-bg-2.jpg
date: 2019-06-20T16:25:10.328Z
seo:
  title: How to choose fonts for web design projects?
  description: >-
    Choosing a font for your project is not an easy thing to do. Finding a
    perfect match, defining your audience and understanding the purpose are the
    most important things you need to have an eye for.
---
Choosing a font for your project is not an easy thing to do. Finding a perfect match, defining your audience and understanding the purpose are the most important things you need to have an eye for.

## Brand identity

Choosing a font must match the brand identity - a font design might look nice, but if it doesn’t fit the purpose, scrap it! Focus on picking a font design that has the characteristics you want to represent your website and brand.

## Adaptation

Choose a font that is versatile - You will be using the same font(s) throughout your website, and your website could be viewed on different devices with different screen sizes. Ask yourself these questions: Does it look good in different weight like bold, italics or normal? Does it look good in various sizes?

## Right audience

Your font design should match the type of customer you want to attract - What is the ideal age group? What is the preferred gender? What profession is he/she in? Does he/she have more/less disposable income?

Want to learn more about web design? Follow us on [LinkedIn](https://www.linkedin.com/company/18723408) & [Facebook](https://www.facebook.com/devhance/)
