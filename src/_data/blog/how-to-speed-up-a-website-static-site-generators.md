---
templateKey: blog-post
title: 'How to speed up a website: Static Site Generators'
description: >-
  Devhance’s team did a quick research and got the results that 8 out of 10
  customers are not satisfied with the current web performance. Why? They lose
  clients online. Here is the solution how to speed up a website with Static
  Site Generators.
image: /images/cms/12.jpg
bgImage: /images/cms/cover-for-blog-3.png
date: 2020-05-04T09:37:05.326Z
seo:
  title: 'How to speed up a website: Static Site Generators'
  description: >-
    Devhance’s team did a quick research and got the results that 8 out of 10
    customers are not satisfied with the current web performance. Why? They lose
    clients online. Here is the solution how to speed up a website with Static
    Site Generators.
---
Devhance’s team did a quick research and got the results that 8 out of 10 customers are not satisfied with the current web performance. Why? They lose clients online. Here is the solution how to speed up a website with Static Site Generators.

## What are Static Site Generators (SSG)?

A new trend is invading in the web industry faster than we can imagine. A huge amount of businesses are migrating from ready solutions like Wordpress in order to increase the business value. 

Static site generators offer a golden mean between hand-coded static sites and websites with integrated CMS. It is a tool that helps you build static pages out of the input files. It takes your content (from a headless CMS for example), applies a selected template, and generates static HTML pages out of it. If a user requests a page, our server finds the matching file and sends it back to the user. All these processes guarantee a superior-fast web performance and reliability.

## Security benefits of SSG

According to [zdnet.com](https://www.zdnet.com/article/wordpress-accounted-for-90-percent-of-all-hacked-cms-sites-in-2018/), WordPress accounted for 90 percent of all hacked CMS sites in 2018. Feels like an “emergency call” for business owners working with e-commerce especially.

Since static sites consist of static files with no database, there’s no way for hackers to do SQL injections or any kind of scripting attacks. There are no plugins like in Wordpress websites that can also stand on the way for security.

Without any hesitation we would say that static sites are impossible to hack. No running code, no database and no other vulnerabilities to exploit. 

## Cost-effective solutions

Just think about the number of plugins you need to install on Wordpress websites to customize it, run it successfully and not taking into account further maintenance, hosting cost and other expenses. And what about backups? Our clients told us that before they migrated to Static Site Generators, they do backups every week because something went wrong, it's a disaster!

Running a static site is inexpensive – it costs pennies per month to run on services like Amazon S3. There are a various number of static site generators that offer plans for individual contributors, small businesses and corporations.

So if you still think – is it worth is to migrate from Wordpress or other similar platform, Devhance’s team would highly recommend to do it right now. It would not only save your money, but it can also show a new perspective direction for your business.



If you want to learn more about Static Site Generators, follow us on: [LinkedIn](https://www.linkedin.com/company/devhance/), [Facebook](https://www.facebook.com/devhance/) & [Behance](https://www.behance.net/devhance)!
