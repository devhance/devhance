---
templateKey: case-page
cardTitle: Nonacare
pageTitle: Nonacare
caseDescription: Nonacare is the leading corporate childcare platform. They
  enable childcare employment benefits for any company size to increase people's
  engagement, reduce employee absenteeism and make working parents work more
  successful.
caseImage: /images/cms/case-nonacare-card.png
caseLogo: /images/cms/case-nonacare-logo.svg
caseLink: https://www.behance.net/gallery/96013327/Nona-Care-Online-booking-platform-UXUI
caseLinkName: Explore Nonacare in details +
overview:
  image: /images/cms/case-overview-nonacare.png
  text: The only platform in Europe to fully automate the corporate childcare.
    With child safety being the first priority Nonacare gives unlimited freedom
    to working parents and top quality corporate culture to companies.
challenge:
  list:
    - text: Create a design guideline that starts from finding fonts, color palette
        and ends up with interactive mockup for further development.
    - text: "Provide UX solutions to customer's dashboard: a step-by-step process of
        searching, selecting and booking a nanny according to customer's
        preferences."
    - text: Design a various number of icons that makes a process of navigation
        through website more intuitive and quicker to understand.
  text: Devhance's team was responsible for the UX/UI of the platform including
    graphic design assets, animations and consulting on possible improvements.
solution:
  image: /images/cms/case-nonacare-solution.png
  list:
    - text: After few months of efficient work our team delivered the design guideline
        for product owner and developers. Going through a lot of iterations and
        feedback from the CEO we used bright illustrations and graphic elements
        that support the theme of the website.
    - text: We designed a 7-stage concept that allows to find a nanny, select nanny
        that fits you best, choose a date, enter your home address & detailed
        info about the kid, make an order, provide your personal details and in
        the end you just need to pay and get a notification on your email.
    - text: Colorful and easy-to-understand illustrations were made as a part of UI
        design for the platform. Adobe Photoshop + Adobe Illustrator were the
        main tools that helped us to meet the expectations.
  text: 'We delivered designs according to technical requirements with the
    opportunity to be a part of future enhancements of the platform in terms of
    UX/UI:'
review:
  author: Unknown, Unknown at Nonacare
  description: Nonacare – the leading corporate childcare platform
  text: “Unfortunately we didn't get a feedback from Nonacare team, but we would
    love to share one when some of executives will contact us in closest
    future.”
date: 2020-11-26T17:46:52.788Z
seo:
  description: Nonacare is the leading corporate childcare platform. They enable
    childcare employment benefits for any company size to increase people's
    engagement, reduce employee absenteeism and make working parents work more
    successful.
  title: Nonacare
---
