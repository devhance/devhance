---
templateKey: case-page
cardTitle: Languages4Life
pageTitle: Languages4Life
caseDescription: Languages4Life is a modern, student-focused language school in
  the heart of Barcelona with friendly and professional native teachers to help
  you learn Spanish. School offers immersive, small-group English and Spanish
  courses that embrace core ideas of mutual respect, attentive staff and
  personalized learning.
caseImage: /images/cms/languages4life.jpg
caseLogo: /images/cms/l4l-logo.png
caseLink: https://www.languages4life.com/en/
caseLinkName: Explore Languages4Life in details +
overview:
  text: Languages4Life, founded in Barcelona in 2011, specializes in high-quality
    Spanish lessons as well as Professional English and Exam Preparation. With
    options for English and Spanish learners of all skill levels and abilities,
    their state-of-the-art facility in Barcelona has a course with your name on
    it.
  image: /images/cms/nona-2.png
challenge:
  text: "To rebuild a website by providing end-to-end development with the
    following requirements: "
  list:
    - text: Give a new look for the current website by enhancing UX/UI design. Follow
        the corporate identity guidelines to provide a new version of the
        design.
    - text: Integrate an e-commerce model to the project in order to decrease the
        number of interactions between a company representative and a customer.
    - text: Improve overall web performance by using progressive stack of
        technologies.
  image: /images/cms/languages4life.jpg
solution:
  text: "Devhance’s team was working in close cooperation with the client and
    after 2.5 month of productive work the project is successfully running
    with:"
  image: /images/cms/nona-care-3-2-.png
  list:
    - text: Updated UX/UI design according to the client’s requirements.
    - text: A website’s architecture was created from scratch to avoid critical bugs
        in the future. JAMstack was chosen as the solution for a better web
        performance. Stripe was integrated into the web as the payment
        processor.
    - text: Student portal feature was designed and developed. This feature allows
        students to check all the needed information about the course they are
        registered for.
    - text: CRM integration allows company representatives to get the information
        about the students, monitor important statistics and communicate with
        potential customers.
    - text: "CMS integration helps to manage the content on a website: change course
        card’s info, add/edit/delete blog articles and etc. Prismic is used as
        the CMS for this project."
review:
  text: "\"We can't thank Andrew and his team enough, they delivered an impressive
    e-commerce website which will have a positive impact on our business. The
    design was professional, the website works perfectly and is super fast. If
    you have struggled to manage / build your own website then I highly
    recommend these professionals! We're also looking forward to working with
    Andrii and his team with ongoing maintenance and other website projects.
    10/10\""
  author: Richard Davie, Director at Languages4Life
  description: Languages4Life - student-focused language school in Barcelona
date: 2020-12-04T17:45:34.163Z
seo:
  title: "Languages4Life: Spanish & English classes in Barcelona"
  description: "Languages4Life is a modern, student-focused language school in the
    heart of Barcelona with friendly and professional native teachers to help
    you learn Spanish. "
---
