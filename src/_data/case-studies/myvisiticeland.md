---
templateKey: case-page
cardTitle: MyVisitIceland
pageTitle: My Visit Iceland
caseDescription: My Visit Iceland is an official website of National
  Organization of Tourism in Iceland – the country of contrasts and a place of
  striking natural beauty. A huge variety of tours and experiences will not
  leave you indifferent.
caseImage: /images/cms/case-myvisiticeland-card.png
caseLogo: /images/cms/case-myvisiticeland-logo.svg
caseLink: https://www.behance.net/gallery/87010715/Visit-Iceland-Travel-Website
caseLinkName: Explore MyVisitIceland in details +
overview:
  image: /images/cms/case-overview-myvisiticeland.png
  text: My Visit Iceland is a travel website of National Organization of Tourism
    in Iceland. The main focus of MyVisitIceland is to provide detailed
    information and booking option of available tours, places or accommodations
    for country’s guests.
challenge:
  image: /images/cms/case-myvisiticeland-overview.jpg
  list:
    - text: Provide new UI design that meets company's requirements in terms of
        branding
    - text: Improve UX design, make it more intuitive focusing on a specific group of
        customers
    - text: Design a “favorite” feature, where guests can “favorite” tours,
        accommodations or interesting places
    - text: Integrate a CMS with a blog crawler that allows to provide previews and
        fetch data from source links
  text: The main goal was to provide end-to-end development of the website with a
    fresher look and better user experience concept.
solution:
  image: /images/cms/iceland-new.png
  list:
    - text: New & fresh UI experience was delivered according to predefined color
        palette. Each destination has its own color, which is subconsciously
        associated with cardinal points in Iceland. For instance, East of
        Iceland is associated with dark-green color because of forests, vast
        expanses and green narrow fjords. 3 shades of blue were chosen as the
        main accent colors. These colors remind us about glaciers and cold in
        Iceland.
    - text: "Devhance's team improved UX concept by adding additional subnavigation
        and moving all the destinations to separate pages. Also we changed a
        structure of the web: activities, restaurants and accommodations are
        different categories that have their own pages. The value of a new UX
        design is concentrated on showing to customer a variety of tours,
        restaurants and accommodations with the option to book in minute, add
        them to favorite and never lose."
    - text: We designed a “favorite” feature, that allows guests to save favorite
        tours, places or accommodations and send them by email. To make this
        feature easy-to-use, we defined a local storage, where all the info is
        saved, so the number of iterations from user’s side is decreased and
        guests don’t need to register or login again.
    - text: CMS integration is under development and soon will be available on the
        main server. Website is on beta test right now.
  text: 'The website is under development, however we are ready to share what is
    done and what problems are solved:'
review:
  author: Höskuldur Jónsson, Project Manager at Ferdavefir
  description: MyVisitIceland – Travel guide website for Iceland
  text: “The new site is a vast improved on the previous iteration and we are
    happy with the result. The efficient workflow was complimented by Devhance's
    ability to quickly identify and rectify issues. They are very quick to
    answer requests and put them into effect. I never had to wait a few days for
    a request to be answered, researched and implemented.”.
date: 2020-12-02T17:46:08.929Z
seo:
  description: My Visit Iceland is an official website of National Organization of
    Tourism in Iceland – the country of contrasts and a place of striking
    natural beauty. A huge variety of tours and experiences will not leave you
    indifferent.
  title: My Visit Iceland
---
