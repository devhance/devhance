---
templateKey: case-page
cardTitle: BE
pageTitle: BE - be a better you for a better tomorrow
caseDescription: BE is a platform that provides an opportunity for people from
  all walks of life to build and own a business from their smartphones while
  living their best life. It is available for everyone, regardless of the
  background, skill-set and experience.
caseImage: /images/cms/befactor.jpg
caseLogo: /images/cms/befactor-logo.png
caseLink: https://berules.com/
caseLinkName: Explore BE in details +
overview:
  text: BE’s mission is to create a place of work for 1 million people from their
    smartphone. Using the cutting-edge Artificial Intelligence, they have made a
    suite of products and apps for education, travel, telecommunications,
    transportation and other industries.
  image: /images/cms/top-stories.png
challenge:
  text: Provide a full cycle of web development using client’s Invision design
    mockups.
  image: /images/cms/bef.jpg
  list:
    - text: Website must be multilingual with an option to expand this feature
    - text: Gatsby (React.js) is a stack for a front-end development
    - text: Advise on choosing a CMS that will help to easily create/manage a huge
        amount of content
    - text: Responsiveness and cross-browser compatibility must be provided for all
        possible devices
solution:
  text: "Within short time frames Devhance’s team has successfully completed the
    work and here is what we’ve got:"
  image: /images/cms/bef-solution.png
  list:
    - text: Fully responsive and cross-browser compatibility tested website was
        delivered according to client’s Invision design mockups.
    - text: Weglot plugin was integrated to this project as the multilingual feature
        with an option to be expanded in the future.
    - text: Prismic was chosen as the content management system that allows to easily
        create a lot of content.
    - text: BE's web architecture was created from scratch with a stack of Gatsby
        (React.js), Node.js, MongoDB and AWS (deploy).
review:
  author: Ehsaan Islam, CTO at BE
  description: BE - be a better you for a better tomorrow.
  text: '"Devhance are experienced and extremely talented at what they take! They
    know how to deal with people and advise on possible enhancements. Despite
    the upcoming deadline they were sharp and consistent with the updates. Will
    be working with them on other web projects!"'
date: 2020-12-06T17:45:11.248Z
seo:
  title: BE - be a better you for a better tomorrow
  description: BE is a platform that provides an opportunity for people from all
    walks of life to build and own a business from their smartphones while
    living their best life. It is available for everyone, regardless of the
    background, skill-set and experience.
---
