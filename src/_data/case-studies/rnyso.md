---
templateKey: case-page
cardTitle: RNYSO
pageTitle: Russian National Youth Symphony Orchestra
caseDescription: Russian National Youth Symphony Orchestra is the unique musical
  collective and the largest youth project in the orchestral field. Since
  September 2018 with the support of the Presidential Grants Foundation and the
  Ministry of Culture of the Russian Federation it was developed as  a part of
  the national project “Culture”.
caseImage: /images/cms/case-rnyso-card2.png
caseLogo: /images/cms/case-rnyso-logo.svg
caseLink: https://rnmso.ru/
caseLinkName: Explore RNYSO in details +
overview:
  image: /images/cms/case-overview-rnyso1.png
  text: 'RNYSO is an integral part of the concert life in Russian Federation: in
    its first season (2018/19), it held 40 concerts, performing in 13 cities of
    the country – from St. Petersburg to Irkutsk, from Vologda to Nizhny
    Novgorod. These concerts have been attended by more than 40,000 listeners.'
challenge:
  list:
    - text: Find the right tech stack for the project that will allow to increase a
        web performance at least 2x times.
    - text: Develop features called "calendar of concerts" and "poster" to see all the
        info about concert date, time, place and who will lead the orchestra.
    - text: Develop "news" blog crawler as a part of content management system.
  text: As design mockups, access to old web and REST API were provided by the
    client, our goal was to redevelop a website with Server Side Rendering (SSR)
    for better SEO and overall web performance.
solution:
  image: /images/cms/rnyso-solution.png
  list:
    - text: We worked with Next.js (React.js) as the main technologies for the project
        that helped us to increase a web performance by 147% comparing with the
        old version of the website. Also Devhance's team enabled compression of
        the code, images and defined a cache validator for better SEO. The
        average time for loading any page is between 1-2 seconds when you are
        fully loaded the website first time.
    - text: The features called "calendar of concerts" and "poster" were developed and
        connected to the existing CRM, so the RNYSO team can add/edit/delete any
        available data. Our team made changes to UX due to new technical
        requirements in order to implement front-end and back-end modules.
    - text: The blog crawler as the news section was developed and connected to
        existing content management system.
  text: "Devhance's team managed to beat all the challenges despite the tight
    deadlines and some issues with hosting provider:"
review:
  author: D. Bochkareva, PR Manager at RNYSO
  description: RNYSO – Russian National Youth Symphony Orchestra
  text: “Devhance did a tremendous work by speeding up our website. Web pages were
    loading quite slow and we wanted to improve it. After 4 weeks I was really
    impressed with the new result. Devhance didn't only provide redevelopment,
    they also fixed a lot of things that weren't working before. Orchestra staff
    wants to say a huge thanks for the practical approach and work done!”.
date: 2020-11-30T17:46:33.383Z
seo:
  description: Russian National Youth Symphony Orchestra is the unique musical
    collective and the largest youth project in the orchestral field. Since
    September 2018 with the support of the Presidential Grants Foundation and
    the Ministry of Culture of the Russian Federation it was developed as  a
    part of the national project “Culture”.
  title: RNYSO
---
