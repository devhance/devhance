---
templateKey: case-page
cardTitle: Harver
pageTitle: Harver
caseDescription: "Harver is #1 pre-hiring and pre-employment assessment platform
  for different companies all around the world. A wide variety of scientifically
  proven pre-employment assessments that are easy to setup and customize to any
  hiring needs."
caseImage: /images/cms/case-harver-card.png
caseLogo: /images/cms/case-harver-logo.svg
caseLink: www.harver.com
caseLinkName: www.harver.com
overview:
  image: /images/cms/harver-overview.png
  text: Harver is a pre-hiring and pre-employment assessment platform, that
    enables innovative companies around the world to hire better, faster. Harver
    helps to automate laborious parts of your pre-hiring and selection process
    and decreases time-to-hire.
challenge:
  image: /images/cms/case-harver-card.png
  list:
    - text: UX solution for pre-hiring and pre-employment assessments that will give
        more options for making a choice to the right candidate
    - text: Simplify the candidate’s interviewing process and expand functionalities
        in terms of creating testing flows
    - text: Provide new UI design concept with predefined color pallette
  text: The main goal was to improve UX design of current platform, make it more
    intuitive and to expand the number of functionalities according to provided
    technical requirements. The second mission was to get rid of dark colors in
    terms of UI design and make them more pleasurable.
solution:
  image: /images/cms/harver-solution.png
  list:
    - text: Number of pre-employment assessments were increased from 4 to 12. It helps
        to measure more efficiently candidate’s cultural preference and compare
        it to the organization’s culture to ensure cultural fit.
    - text: Because of video era, a new feature called “a video interview module” was
        designed. It gives an opportunity for candidates to present themselves
        to the company and to notice even remote candidates that apply.
    - text: All platform’s screens were divided into separate dashboards with sub
        navigation tabs and step-by-step instruction’s tooltips that help to
        build custom testing flows, connect them to the vacancies and start
        hiring in a data-driven way.
    - text: We got rid of dark accent colors and replace them with light-blue which is
        a perfect fit for the white backgrounds with gray box shadow and black
        text content inside of it. Light green as the second accent color was
        chosen for decoration elements, graph’s bars to give a fresh look for
        dashboards.
  text: "Devhance’s team of highly-skilled UX/UI designers found the solutions for
    the challenges and here is what we’ve got:"
review:
  author: Phil Mander, Head of Engineering at Harver
  description: Harver – pre-employment assessment platform
  text: “Devhance are good at what they do. They were able to exceed our
    expectation with minimum information. We almost want to keep them to
    ourselves, but that would not be fair to such a hard working team like
    Devhance. Will definitely work with them again”.
date: 2020-11-20T17:47:11.337Z
seo:
  description: "Harver is #1 pre-hiring and pre-employment assessment platform for
    different companies all around the world. A wide variety of scientifically
    proven pre-employment assessments that are easy to setup and customize to
    any hiring needs."
  title: Harver
---
