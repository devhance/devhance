---
templateKey: case-page
cardTitle: TEFL Iberia
pageTitle: TEFL Iberia
caseDescription: TEFL Iberia is a modern training school in Barcelona, Spain
  where you can learn English as a Foreign Language. It’s not just a school,
  their student support team helps with accommodation, visas, bureaucracy and
  more.
caseImage: /images/cms/tefl-iberia.jpg
caseLogo: /images/cms/logo-new.png
caseLink: https://tefl-iberia.com/
caseLinkName: Explore TEFL Iberia in details +
overview:
  image: /images/cms/overview-tefl.png
  text: "TEFL is an acronym for Teaching English as a Foreign Language. Also known
    as TESOL, ELT or teaching ESL: English to non-native speakers. Comfortable
    classrooms, interactive whiteboards, beautiful library, up-to-date MAC
    computers all make it the perfect home for TEFL Iberia."
challenge:
  text: End-to-end development that includes prototyping, designing, coding and
    testing.
  list:
    - text: >
        The objective was to get the website users to complete the application
        form, so the school will get more leads
    - text: SEO optimized and mobile-friendly website with uncommon features
    - text: New CRM + CMS integration, create better client's interface for managing
        leads
  image: /images/cms/tefl-iberia.jpg
solution:
  text: "Devhance’s team finished the project successfully and after 2 months of
    dedication we’ve got:"
  image: /images/cms/solution-tefl.png
  list:
    - text: New, simple and clean UX/UI design that helps website users to choose and
        sign up for the course in few steps.
    - text: “Monday.com” was chosen as a new CRM for this project.
    - text: As Prismic was used as a content management system in the first client’s
        project, there were no doubts about it when the development of TEFL
        Iberia project started.
    - text: JAMstack helped our team to create SEO optimized and mobile-friendly web.
review:
  text: '"Devhance was able to revamp the website successfully. The results were a
    more seamless user experience and a visually aesthetic layout and design.
    Stakeholders were delighted about the easier navigation and
    user-friendliness of the platform."'
  author: Richard Davie, Director at TEFL Iberia
  description: TEFL Iberia - learn English as a Foreign Language
date: 2020-12-07T17:43:55.142Z
seo:
  title: TEFL Iberia
  description: TEFL Iberia is a modern training school in Barcelona, Spain where
    you can learn English as a Foreign Language. It’s not just a school, their
    student support team helps with accommodation, visas, bureaucracy and more.
---
