import React, { Component } from 'react';
import { navigate } from 'gatsby-link';
import Recaptcha from 'react-google-recaptcha';

const RECAPTCHA_KEY = '6LddEN4UAAAAAKxX9bNgzFYusO4bQcFEwzK-EvxR';
const recaptchaRef = React.createRef();

function encode(data) {
  return Object.keys(data)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
    .join('&');
}
class Contacts extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (e) => {
    const { target } = e;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;

    this.setState({
      [name]: value,
    });
  };

  handleRecaptcha = (value) => {
    this.setState({ recaptcha: value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const form = e.target;

    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({
        'form-name': form.getAttribute('name'),
        ...this.state,
      }),
    })
      .then(() => navigate(form.getAttribute('action')))
      .catch((error) => alert(error));
  };

  render() {
    const { name, email, purpose, agreement, recaptcha } = this.state;
    return (
      <div className="container" itemScope itemType="http://schema.org/Organization">
        <div className="row">
          <div className="col-12 col-lg-8 col-md-8 hire-us">
            <div className="section-title">Hire us</div>

            <form
              name="contact"
              method="POST"
              action="/contacts/thanks/"
              data-netlify="true"
              onSubmit={this.handleSubmit}
            >
              <div className="user-data">
                <label htmlFor="name">Name</label>
                <input
                  onChange={this.handleChange}
                  type="text"
                  name="name"
                  id="user-name"
                  aria-label="name"
                  required
                />
                <label htmlFor="email">Email</label>
                <input
                  onChange={this.handleChange}
                  type="email"
                  name="email"
                  id="user-email"
                  aria-label="email"
                  required
                />
                <label htmlFor="purpose">Purpose</label>
                <input
                  onChange={this.handleChange}
                  type="text"
                  name="purpose"
                  id="user-purpose"
                  aria-label="purpose"
                  required
                />
                <label htmlFor="message">Message (Optional)</label>
                <textarea
                  onChange={this.handleChange}
                  cols="2"
                  rows="2"
                  name="message"
                  id="user-message"
                  aria-label="message"
                />
              </div>
              <div className="agreement d-flex">
                <label className="box" htmlFor="agreement">
                  <input
                    onChange={this.handleChange}
                    type="checkbox"
                    name="agreement"
                    aria-label="agreement"
                    required
                  />
                  <span className="checkmark" />
                </label>
                <span className="description">I agree to have my personal data processed.</span>
              </div>

              {(name || email || purpose || agreement) && (
                <div className="captcha">
                  <Recaptcha
                    ref={recaptchaRef}
                    sitekey={RECAPTCHA_KEY}
                    onChange={this.handleRecaptcha}
                  />
                </div>
              )}

              <div className="submit-form d-flex align-items-center">
                <button className="btn btn--cyan" type="submit" disabled={!recaptcha}>
                  Send
                </button>
                <span>We reply within 24h</span>
              </div>
            </form>
          </div>

          <div className="col-12 col-lg-4 col-md-4 contact">
            <div className="section-title section-title--white">Contact</div>
            <div className="locations">
              <h6>Ukraine</h6>
              <p className="mb">Kyiv</p>
              <h6>Poland</h6>
              <p>Warsaw</p>
            </div>
            <a className="mail" href="mailto:hello@devhance.co" itemProp="email">
              hello@devhance.co
            </a>
            <div className="social d-flex flex-column">
              <a
                href="https://www.facebook.com/devhance/"
                target="_blank"
                rel="noopener noreferrer"
                itemProp="url"
              >
                Facebook
              </a>
              {/* <a
                href="https://www.instagram.com/devhance.co/"
                target="_blank"
                rel="noopener noreferrer"
                itemProp="url"
              >
                Instagram
              </a> */}
              <a
                href="https://www.linkedin.com/company/18723408/"
                target="_blank"
                rel="noopener noreferrer"
                itemProp="url"
              >
                LinkedIn
              </a>
              <a
                href="https://www.behance.net/devhance"
                target="_blank"
                rel="noopener noreferrer"
                itemProp="url"
              >
                Behance
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Contacts;
