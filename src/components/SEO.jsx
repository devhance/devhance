import React from 'react';
import { Helmet } from 'react-helmet';

import config from '../../data/SiteConfig';

const SEO = ({ title, description, image, url, postSEO, postNode }) => {
  const seoTitle = title
    ? `${title} – ${config.siteTitleShort}`
    : `${config.siteTitle} – ${config.siteTitleShort}`;
  const seoDescription = description || config.siteDescription;
  const seoImage = image ? `https://devhance.co${image}` : config.siteImage;
  const seoURL = url || config.siteUrl;

  const authorJSONLD = {
    '@type': 'Person',
    name: config.userName,
    email: config.userEmail,
    address: config.userLocation,
  };
  const logoJSONLD = {
    '@type': 'ImageObject',
    url: seoImage,
  };
  const schemaOrgJSONLD = [
    {
      '@context': 'http://schema.org',
      '@type': 'Organization',
      url: config.siteUrl,
      name: seoTitle,
      alternateName: config.siteTitleShort,
    },
  ];
  if (postSEO) {
    schemaOrgJSONLD.push(
      {
        '@context': 'http://schema.org',
        '@type': 'BreadcrumbList',
        itemListElement: [
          {
            '@type': 'ListItem',
            position: 1,
            item: {
              '@id': seoURL,
              name: seoTitle,
              image: seoImage,
            },
          },
        ],
      },
      {
        '@context': 'http://schema.org',
        '@type': 'BlogPosting',
        url: seoURL,
        name: seoTitle,
        alternateName: config.siteTitleAlt ? config.siteTitleAlt : '',
        headline: seoTitle,
        image: { '@type': 'ImageObject', url: seoImage },
        author: authorJSONLD,
        publisher: {
          ...authorJSONLD,
          '@type': 'Organization',
          logo: logoJSONLD,
        },
        datePublished: new Date(postNode.date).toISOString().slice(0, 10),
        description: seoDescription,
      }
    );
  }

  return (
    <Helmet>
      {/* General tags */}
      <html lang="en" />
      <link rel="alternate" hrefLang="en" />
      <title>{seoTitle}</title>
      <meta name="title" content={seoTitle} />
      <meta name="description" content={seoDescription} />
      <meta name="image" content={seoImage} />

      {/* Schema.org tags */}
      <script type="application/ld+json">{JSON.stringify(schemaOrgJSONLD)}</script>

      {/* OpenGraph tags */}
      <meta property="og:title" content={seoTitle} />
      <meta property="og:description" content={seoDescription} />
      <meta property="og:type" content={postSEO ? 'article' : 'website'} />
      <meta property="og:url" content={seoURL} />
      <meta property="og:locale" content="en" />
      <meta property="og:site_name" content={config.siteTitleShort} />
      <meta property="og:image" content={seoImage.replace('https', 'http')} />
      <meta property="og:image:secure_url" content={seoImage} />
      <meta property="og:image:width" content="512" />
      <meta property="og:image:height" content="512" />
      <meta name="og:email" content={config.userEmail} />
      <meta property="fb:app_id" content={config.siteFBAppID ? config.siteFBAppID : ''} />

      {/* Twitter Card tags */}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={seoTitle} />
      <meta name="twitter:description" content={seoDescription} />
      <meta name="twitter:site" content={config.userTwitter || config.siteUrl} />
      <meta name="twitter:image" content={seoImage} />
      <meta name="twitter:creator" content={config.userTwitter || config.siteUrl} />
      <meta name="twitter:domain" content="devhance.co" />
      <meta name="twitter:url" content={seoURL} />

      <meta name="google-site-verification" content="Axn0Zpilj7W4CB7lcTGhg3cDXJGklE1ZPfk9Y5JiSX0" />
      <meta name="p:domain_verify" content="2e4e9b48c4afd86f8f14c73e533f0827" />
      <meta name="p:domain_verify" content="6f9cffa24671e94c3eeabe9494abbc9e" />
      <link rel="preconnect" href="https://cdnjs.cloudflare.com" />
      <link rel="preconnect" href="https://serve.albacross.com/track.js" />
      <link
        rel="stylesheet"
        type="text/css"
        charset="UTF-8"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
      />
    </Helmet>
  );
};

export default SEO;
