import React from 'react';

import SEO from './SEO';
import Header from './Header';
import Footer from './Footer';

// Styles
import '../styles/main.scss';

const Layout = ({ title, description, image, url, isMainPage, children, postNode, postSEO }) => {
  return (
    <>
      <SEO
        title={title}
        description={description}
        image={image}
        url={url}
        isMainPage={isMainPage}
        postNode={postNode}
        postSEO={postSEO}
      />
      <Header isMainPage={isMainPage} />
      <div className="layout-content">{children}</div>
      <Footer />
    </>
  );
};

export default Layout;
