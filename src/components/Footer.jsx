import React from 'react';
import { Link } from 'gatsby';

const albacross = `<script type="text/javascript" async>
(function(a,l,b,c,r,s){
_nQc=c,r=a.createElement(l),s=a.getElementsByTagName(l)[0];
r.async=1;
r.src=l.src=("https:"==a.location.protocol?"https://":"http://")+b;
s.parentNode.insertBefore(r,s);})
(document,"script","serve.albacross.com/track.js","89307323");
</script>`;

const Footer = () => (
  <div className="footer">
    <div className="container footer__content">
      <span>© Devhance {new Date().getFullYear()}. All rights reserved.</span>
      <Link to="/terms-of-use/">Terms & Privacy</Link>
    </div>
    <div dangerouslySetInnerHTML={{ __html: albacross }} />
  </div>
);

export default Footer;
