import React, { Component } from 'react';
import { Link } from 'gatsby';
import AnchorLink from 'react-anchor-link-smooth-scroll';

class Header extends Component {
  state = {
    fixHeder: false,
    showMobileMenu: false,
  };

  componentDidMount() {
    window.addEventListener(`scroll`, this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener(`scroll`, this.handleScroll);
  }

  handleScroll = () => {
    const scrolled = window.pageYOffset || document.documentElement.scrollTop;

    this.setState({
      fixHeder: scrolled > 40,
    });
  };

  handleMobileMenu(mobileState) {
    this.setState({
      showMobileMenu: mobileState,
    });
  }

  render() {
    const { isMainPage = false } = this.props;
    const { fixHeder, showMobileMenu } = this.state;

    return (
      <div
        className={fixHeder ? 'header header--fix' : 'header'}
        itemScope
        itemType="http://schema.org/Organization"
      >
        <div className="col-2 col-lg-2 d-flex align-items-center">
          <Link to="/" itemProp="url">
            <img src="/images/svgs/logo.svg" alt="Devhance" title="Devhance" itemProp="logo" />
          </Link>
        </div>
        <div className="navigation-mobile col-10 d-block d-md-none">
          <button onClick={() => this.handleMobileMenu(!showMobileMenu)}>
            <img src="/images/svgs/menu.svg" alt="Menu icon" />
          </button>
        </div>
        <div className="navigation-desktop col-10 col-lg-10 d-flex justify-content-end align-items-center">
          <nav>
            <Link to="/about/" itemProp="url">
              About
            </Link>

            {isMainPage ? (
              <AnchorLink href="#expertise-id" itemProp="url">
                Expertise
              </AnchorLink>
            ) : (
              <Link to="/#expertise-id" itemProp="url">
                Expertise
              </Link>
            )}

            <Link to="/case-studies/" itemProp="url">
              Case Studies
            </Link>
            <Link to="/blog/" itemProp="url">
              Blog
            </Link>
          </nav>
          <Link to="/contacts/" itemProp="url">
            <button className="btn btn--trasparent btn--white">Contact Us</button>
          </Link>
        </div>

        {showMobileMenu && (
          <div className="mobile-menu">
            <button className="close-btn" onClick={() => this.handleMobileMenu(false)}>
              <img src="/images/svgs/close-mobile-nav.svg" alt="Close icon" />
            </button>
            <nav>
              <Link to="/">Home</Link>
              {/* <Link to="/about" itemProp="url">
                About
              </Link> */}
              {isMainPage ? (
                <AnchorLink href="#expertise-id" onClick={() => this.handleMobileMenu(false)}>
                  Expertise
                </AnchorLink>
              ) : (
                <Link to="/#expertise-id" onClick={() => this.handleMobileMenu(false)}>
                  Expertise
                </Link>
              )}
              <Link to="/case-studies/" onClick={() => this.handleMobileMenu(false)}>
                Case Studies
              </Link>
              <Link to="/blog/" onClick={() => this.handleMobileMenu(false)}>
                Blog
              </Link>
              <Link to="/contacts/" onClick={() => this.handleMobileMenu(false)}>
                Contacts
              </Link>
            </nav>
          </div>
        )}
      </div>
    );
  }
}

export default Header;
