const _ = require('lodash');
const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');
const { fmImagesToRelative } = require('gatsby-remark-relative-images');

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions;

  return graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            id
            fields {
              slug
            }
            frontmatter {
              templateKey
            }
          }
        }
      }
    }
  `).then((result) => {
    if (result.errors) {
      result.errors.forEach((e) => console.error(e.toString()));
      return Promise.reject(result.errors);
    }

    const data = result.data.allMarkdownRemark.edges;

    data.forEach((edge) => {
      const { id } = edge.node;
      const { templateKey } = edge.node.frontmatter;

      switch (templateKey) {
        case 'blog-post':
          createPage({
            path: edge.node.fields.slug,
            component: path.resolve(`src/templates/blog-post.jsx`),
            // additional data can be passed via context
            context: {
              id,
            },
          });
          break;

        case 'case-page':
          createPage({
            path: edge.node.fields.slug,
            component: path.resolve(`src/templates/case-page.jsx`),
            // additional data can be passed via context
            context: {
              id,
            },
          });
          break;

        default:
          console.error('Unknown template key :(');
      }
    });
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  fmImagesToRelative(node); // convert image paths for gatsby images

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: `slug`,
      node,
      value,
    });
  }
};
